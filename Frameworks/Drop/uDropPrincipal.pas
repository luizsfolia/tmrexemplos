unit uDropPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Actions, Vcl.ActnList,
  Vcl.StdCtrls;

type
  TfrmDropMain = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    MainActionList: TActionList;
    RebuildDatabaseAction: TAction;
    actProducts: TAction;
    actCadClient: TAction;
    actOrder: TAction;
    procedure actProductsExecute(Sender: TObject);
    procedure actCadClientExecute(Sender: TObject);
    procedure actOrderExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDropMain: TfrmDropMain;

implementation

{$R *.dfm}

procedure TfrmDropMain.actCadClientExecute(Sender: TObject);
begin
  TfrmFindDefault.Select<TClient>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'ID';
      Sender.Columns.Add.Caption := 'Name';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TClient)
    begin
      Item.Caption := pItemObject.ID.ToString;
      Item.SubItems.Add(pItemObject.Name);
    end,
    'NAME',
    TfrmCadClient,
    Self);
end;

procedure TfrmDropMain.actOrderExecute(Sender: TObject);
begin
  TfrmFindDefault.Select<TOrders>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'ID';
      Sender.Columns.Add.Caption := 'Date';
      Sender.Columns.Add.Caption := 'Number';
      Sender.Columns.Add.Caption := 'Total';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TOrders)
    begin
      Item.Caption := pItemObject.ID.ToString;
      Item.SubItems.Add(DateToStr(pItemObject.Date));
      Item.SubItems.Add(pItemObject.Number.ToString);
//      Item.SubItems.Add(IntToStr(pItemObject.Quantity));
    end,
    'Number',
    TfrmOrder,
    Self);
end;

procedure TfrmDropMain.actProductsExecute(Sender: TObject);
begin
  TfrmFindDefault.Select<TProduct>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'Name';
      Sender.Columns.Add.Caption := 'Price';
      Sender.Columns.Add.Caption := 'Quantity';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TProduct)
    begin
      Item.Caption := pItemObject.Name;
      Item.SubItems.Add(CurrToStr(pItemObject.Price));
      Item.SubItems.Add(IntToStr(pItemObject.Quantity));
    end,
    'Name',
    TfrmCadProduct,
    Self);
end;

end.
