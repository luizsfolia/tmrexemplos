program VCL_Drop;







uses
  Vcl.Forms,
  uDropPrincipal in 'uDropPrincipal.pas' {frmDropMain},
  ViewFindDefault in 'Views\ViewFindDefault.pas',
  ViewCadDefault in 'Views\ViewCadDefault.pas',
  ViewOrder in 'Views\ViewOrder.pas',
  ViewCadClient in 'Views\ViewCadClient.pas',
  ViewCadProduct in 'Views\ViewCadProduct.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmDropMain, frmDropMain);
  Application.Run;
end.
