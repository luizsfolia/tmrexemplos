unit ViewMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Spring.Persistence.Core.Interfaces, Spring.Persistence.Core.Session,
  System.Actions, Vcl.ActnList, System.Classes, OrderModel, Spring.Collections;

type

  TMySession = class(TSession)
  public
    function GetMyOrder: IList<TOrder>;
  end;

  TfrmMain = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    MainActionList: TActionList;
    RebuildDatabaseAction: TAction;
    actProducts: TAction;
    actCadClient: TAction;
    actOrder: TAction;
    Button5: TButton;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure RebuildDatabaseActionExecute(Sender: TObject);
    procedure actProductsExecute(Sender: TObject);
    procedure actCadClientExecute(Sender: TObject);
    procedure actOrderExecute(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    FConnection: IDBConnection;
    FSession: TMySession;

    procedure DoBuildDatabase;
    procedure DoCheckEntities;
  public
    property Connection: IDBConnection read FConnection;
    property Session: TMySession read FSession;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  Spring.Persistence.Core.DatabaseManager, SQLiteTable3,
  Spring.Persistence.Adapters.SQLite, ProductModel,
  ViewFindDefault, Vcl.ComCtrls, ViewCadProduct, ClientModel, ViewCadClient, ViewOrder, Spring.Persistence.SQL.Commands.Select,
  OrderItemModel, Spring.Persistence.Criteria.Interfaces,
  Spring.Persistence.Criteria.Criterion.SimpleExpression,
  Spring.Persistence.SQL.Types;

{$R *.dfm}

procedure TfrmMain.actCadClientExecute(Sender: TObject);
begin
  TfrmFindDefault.Edit<TClient>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'ID';
      Sender.Columns.Add.Caption := 'Name';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TClient)
    begin
      Item.Caption := pItemObject.ID.ToString;
      Item.SubItems.Add(pItemObject.Name);
    end,
    'CLINAME',
    TfrmCadClient,
    Self);
end;

procedure TfrmMain.actOrderExecute(Sender: TObject);
begin
  TfrmFindDefault.Edit<TOrder>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'ID';
      Sender.Columns.Add.Caption := 'Date';
      Sender.Columns.Add.Caption := 'Number';
      Sender.Columns.Add.Caption := 'Total';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TOrder)
    begin
      Item.Caption := pItemObject.ID.ToString;
      Item.SubItems.Add(DateToStr(pItemObject.Date));
      Item.SubItems.Add(pItemObject.Number.ToString);
//      Item.SubItems.Add(IntToStr(pItemObject.Quantity));
    end,
    'ORDERNUMBER',
    TfrmOrder,
    Self);
end;

procedure TfrmMain.actProductsExecute(Sender: TObject);
begin
  TfrmFindDefault.Edit<TProduct>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'Name';
      Sender.Columns.Add.Caption := 'Price';
      Sender.Columns.Add.Caption := 'Quantity';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TProduct)
    begin
      Item.Caption := pItemObject.Name;
      Item.SubItems.Add(CurrToStr(pItemObject.Price));
      Item.SubItems.Add(IntToStr(pItemObject.Quantity));
    end,
    'PRODNAME',
    TfrmCadProduct,
    Self);
end;

procedure TfrmMain.Button5Click(Sender: TObject);
var
  lItens: IList<TOrder>;
  lITem: TOrder;
  lITemOrder: TOrderItem;
  lOrder: TOrder;
begin


  lOrder := TOrder.Create;
  lOrder.Client := FSession.FindOne<TClient>(1);

  lOrder.Number := 1234;

  lITemOrder := TOrderItem.Create;
  lITemOrder.ProductId := 1;
  lITemOrder.Price := 10;
  lITemOrder.Quantity := 10;

  lOrder.Itens.Add(lITemOrder);


  lITemOrder := TOrderItem.Create;
  lITemOrder.ProductId := 1;
  lITemOrder.Price := 10;
  lITemOrder.Quantity := 10;

  lOrder.Itens.Add(lITemOrder);


  FSession.SaveAll(lOrder);


  lItens := FSession.GetMyOrder;

  for lITem in lItens do
  begin
    Memo1.Lines.Add(lITem.ID.ToString);
    for lITemOrder in lITem.Itens do
    begin
      Memo1.Lines.Add('>' + lITemOrder.ID.ToString + ' - ' + lITemOrder.Product.Name);
    end;
  end;
end;

procedure TfrmMain.DoBuildDatabase;
var
  lDBManager: TDatabaseManager;
begin
  lDBManager := TDatabaseManager.Create(FConnection);
  try
    lDBManager.BuildDatabase;
  finally
    lDBManager.Free;
  end;
end;

procedure TfrmMain.DoCheckEntities;
var
  lDBManager: TDatabaseManager;
begin
  lDBManager := TDatabaseManager.Create(FConnection);
  try
    if (not lDBManager.EntityExists(TProduct)) then
      lDBManager.BuildDatabase;
  finally
    lDBManager.Free;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  FDatabase: TSQLiteDatabase;
begin
  FDatabase := TSQLiteDatabase.Create(Self);
  FDatabase.Filename := 'products.db3';
  FConnection := TSQLiteConnectionAdapter.Create(FDatabase);

  FConnection.AutoFreeConnection := True;
  FConnection.Connect;
  FSession := TMySession.Create(FConnection);

  DoCheckEntities;
end;

procedure TfrmMain.RebuildDatabaseActionExecute(Sender: TObject);
begin
  DoBuildDatabase;
end;

{ TMySession }

function TMySession.GetMyOrder: IList<TOrder>;
var
  lCriterion: ICriterion;
  lItem: TOrder;
  lOrderItem: TOrderITem;
begin
  Result := Self.FindAll<TOrder>;
  for lItem in Result do
  begin
//    lItem.Client := Self.FindOne<TClient>(lItem.ClientId);

    lCriterion := TSimpleExpression.Create('OrderId', lItem.Id, TWhereOperator.woEqual);
    lItem.Itens := Self.FindWhere<TOrderItem>(lCriterion);

    for lOrderItem in lItem.Itens do
      lOrderItem.Product := Self.FindOne<TProduct>(lOrderItem.ProductId);
  end;
end;

end.
