unit ViewCadDefault;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, System.Actions,
  Vcl.ActnList, Vcl.StdCtrls;

type
  TfrmCadDefault = class(TForm)
    pnlActions: TPanel;
    btnOk: TButton;
    btnCancel: TButton;
    MainActionList: TActionList;
    actOk: TAction;
    actCancel: TAction;
    pnlData: TPanel;
    procedure actOkExecute(Sender: TObject);
    procedure actCancelExecute(Sender: TObject);
  private
    FObject : TObject;
  protected
    procedure SetObject(pObject : TObject);
    function GetObjectAs<T : Class, constructor> : T;

    procedure DoGetObjectProperties; virtual;
    procedure DoSetObjectProperties; virtual;
  public
    class function Edit<T : Class, constructor>(const pObject: T): Boolean;
  end;

  TfrmCadDefaultClass = class of TfrmCadDefault;

var
  frmCadDefault: TfrmCadDefault;

implementation

{$R *.dfm}

{ TfrmCadDefault }

procedure TfrmCadDefault.actCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmCadDefault.actOkExecute(Sender: TObject);
begin
  DoSetObjectProperties;
  ModalResult := mrOk;
end;

procedure TfrmCadDefault.DoGetObjectProperties;
begin

end;

procedure TfrmCadDefault.DoSetObjectProperties;
begin

end;

class function TfrmCadDefault.Edit<T>(const pObject: T): Boolean;
var
  lForm: TfrmCadDefault;
begin
  LForm := Self.Create(Application);
  try
    lForm.SetObject(pObject);
    Result := (lForm.ShowModal = mrOk);
  finally
    lForm.Free;
  end;
end;

function TfrmCadDefault.GetObjectAs<T>: T;
begin
  Result := T(FObject);
end;

procedure TfrmCadDefault.SetObject(pObject: TObject);
begin
  FObject := pObject;
  DoGetObjectProperties;
end;

end.
