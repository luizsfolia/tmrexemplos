unit ViewCadClient;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ViewCadDefault, System.Actions,
  Vcl.ActnList, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TfrmCadClient = class(TfrmCadDefault)
    NameEdit: TEdit;
    NameLabel: TLabel;
  private
    { Private declarations }
  protected
    procedure DoGetObjectProperties; override;
    procedure DoSetObjectProperties; override;
  public
    { Public declarations }
  end;

var
  frmCadClient: TfrmCadClient;

implementation

{$R *.dfm}

uses ClientModel;

{ TfrmCadClient }

procedure TfrmCadClient.DoGetObjectProperties;
begin
  inherited;
  NameEdit.Text := GetObjectAs<TClient>.Name;
end;

procedure TfrmCadClient.DoSetObjectProperties;
begin
  inherited;
  GetObjectAs<TClient>.Name := NameEdit.Text;
end;

end.
