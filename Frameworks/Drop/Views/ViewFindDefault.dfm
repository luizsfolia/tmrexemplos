object frmFindDefault: TfrmFindDefault
  Left = 0
  Top = 0
  Caption = 'Find'
  ClientHeight = 389
  ClientWidth = 707
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lstData: TListView
    AlignWithMargins = True
    Left = 3
    Top = 92
    Width = 701
    Height = 222
    Align = alClient
    Columns = <>
    GridLines = True
    OwnerData = True
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnData = lstDataData
    OnDblClick = lstDataDblClick
  end
  object pnlActions: TPanel
    Left = 0
    Top = 0
    Width = 707
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    Caption = 'pnlActions'
    ShowCaption = False
    TabOrder = 1
    object edtFind: TEdit
      Left = 32
      Top = 11
      Width = 257
      Height = 21
      TabOrder = 0
    end
    object btnFind: TButton
      Left = 296
      Top = 8
      Width = 75
      Height = 25
      Action = actFind
      TabOrder = 1
    end
    object btnAdd: TButton
      Left = 32
      Top = 48
      Width = 75
      Height = 25
      Action = actAdd
      TabOrder = 2
    end
    object Button1: TButton
      Left = 113
      Top = 48
      Width = 75
      Height = 25
      Action = actRemove
      TabOrder = 3
    end
    object Button2: TButton
      Left = 194
      Top = 48
      Width = 75
      Height = 25
      Action = actSave
      TabOrder = 4
    end
  end
  object pnlSelect: TPanel
    Left = 0
    Top = 317
    Width = 707
    Height = 53
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 2
    object btnSelect: TButton
      Left = 602
      Top = 12
      Width = 75
      Height = 25
      Action = actSelect
      TabOrder = 0
    end
  end
  object stbStatus: TStatusBar
    Left = 0
    Top = 370
    Width = 707
    Height = 19
    Panels = <
      item
        Width = 250
      end
      item
        Width = 50
      end>
  end
  object MainActionList: TActionList
    OnUpdate = MainActionListUpdate
    Left = 448
    Top = 176
    object actAdd: TAction
      Category = 'Actions'
      Caption = 'Add'
      OnExecute = actAddExecute
    end
    object actRemove: TAction
      Category = 'Actions'
      Caption = 'Remove'
      OnExecute = actRemoveExecute
    end
    object actSave: TAction
      Category = 'Actions'
      Caption = 'Save'
      OnExecute = actSaveExecute
    end
    object actFind: TAction
      Category = 'Actions'
      Caption = 'Find'
      OnExecute = actFindExecute
    end
    object actSelect: TAction
      Category = 'Actions'
      Caption = 'Select'
    end
  end
end
