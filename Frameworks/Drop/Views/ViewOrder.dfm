inherited frmOrder: TfrmOrder
  Caption = 'Order'
  ClientHeight = 454
  ClientWidth = 709
  ExplicitWidth = 725
  ExplicitHeight = 493
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlActions: TPanel
    Top = 375
    Width = 709
    ExplicitTop = 375
    ExplicitWidth = 709
  end
  inherited pnlData: TPanel
    Width = 709
    Height = 375
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 709
    ExplicitHeight = 375
    object QuantityLabel: TLabel
      Left = 16
      Top = 41
      Width = 41
      Height = 13
      Caption = 'Number:'
    end
    object PriceLabel: TLabel
      Left = 16
      Top = 69
      Width = 31
      Height = 13
      Caption = 'Client:'
    end
    object Label1: TLabel
      Left = 16
      Top = 133
      Width = 26
      Height = 13
      Caption = 'Item:'
    end
    object Label2: TLabel
      Left = 40
      Top = 161
      Width = 29
      Height = 13
      Caption = 'Count'
    end
    object NumberSpinEdit: TSpinEdit
      Left = 80
      Top = 38
      Width = 81
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 0
      Value = 1
    end
    object ClientName: TMaskEdit
      Left = 174
      Top = 66
      Width = 309
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
      Text = ''
      TextHint = 'Price'
    end
    object speClientId: TSpinEdit
      Left = 80
      Top = 66
      Width = 81
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 2
      Value = 1
    end
    object lstData: TListView
      AlignWithMargins = True
      Left = 3
      Top = 192
      Width = 703
      Height = 180
      Align = alBottom
      Columns = <>
      GridLines = True
      OwnerData = True
      ReadOnly = True
      RowSelect = True
      TabOrder = 3
      ViewStyle = vsReport
    end
    object speItem: TSpinEdit
      Left = 80
      Top = 130
      Width = 81
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 4
      Value = 1
    end
    object edtITem: TMaskEdit
      Left = 174
      Top = 130
      Width = 309
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 5
      Text = ''
      TextHint = 'Price'
    end
    object SpinEdit1: TSpinEdit
      Left = 80
      Top = 158
      Width = 81
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 6
      Value = 1
    end
    object Button1: TButton
      Left = 174
      Top = 157
      Width = 75
      Height = 25
      Action = actAddItem
      TabOrder = 7
    end
    object Button2: TButton
      Left = 552
      Top = 96
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 8
    end
  end
  inherited MainActionList: TActionList
    object actAddItem: TAction
      Caption = 'Add Item'
      OnExecute = actAddItemExecute
    end
  end
end
