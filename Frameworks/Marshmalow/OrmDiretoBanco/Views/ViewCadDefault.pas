unit ViewCadDefault;

interface

uses
  System.Generics.Collections, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, System.Actions,
  Vcl.ActnList, Vcl.StdCtrls, ViewListDefault, Spring.Collections;

type
  TfrmCadDefault = class(TForm)
    pnlActions: TPanel;
    btnOk: TButton;
    btnCancel: TButton;
    MainActionList: TActionList;
    actOk: TAction;
    actCancel: TAction;
    pnlData: TPanel;
    procedure actOkExecute(Sender: TObject);
    procedure actCancelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FObject : TObject;
    FChilds : TDictionary<string, TfrmListDefault>;
  protected
    procedure SetObject(pObject : TObject);
    function GetObjectAs<T : class, constructor> : T;

    procedure DoRepaint; virtual;
    procedure DoAddChilds; virtual;
    procedure DoAddChild(pKey : string; pFrmList : TfrmListDefault);
    procedure DoRemoveChildSelected(pKey : string);

    procedure DoGetObjectProperties; virtual;
    procedure DoSetObjectProperties; virtual;
  public
    class function Edit<T : class, constructor>(const pObject: T): Boolean;
  end;

  TfrmCadDefaultClass = class of TfrmCadDefault;

var
  frmCadDefault: TfrmCadDefault;

implementation

{$R *.dfm}

{ TfrmCadDefault }

procedure TfrmCadDefault.actCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmCadDefault.actOkExecute(Sender: TObject);
begin
  DoSetObjectProperties;
  ModalResult := mrOk;
end;

procedure TfrmCadDefault.DoAddChild(pKey : string; pFrmList : TfrmListDefault);
begin
  FChilds.Add(pKey, pFrmList);
end;

procedure TfrmCadDefault.DoAddChilds;
begin

end;

procedure TfrmCadDefault.DoGetObjectProperties;
begin

end;

procedure TfrmCadDefault.DoRemoveChildSelected(pKey: string);
var
  lFrmList: TfrmListDefault;
begin
  if FChilds.TryGetValue(pKey, lFrmList) then
  begin
    lFrmList.DeleteSelected;
  end;
end;

procedure TfrmCadDefault.DoRepaint;
var
  lChild: TfrmListDefault;
begin
 for lChild in FChilds.Values do
   lChild.DoRepaint;
end;

procedure TfrmCadDefault.DoSetObjectProperties;
begin

end;

procedure TfrmCadDefault.FormCreate(Sender: TObject);
begin
  FChilds := TDictionary<string, TfrmListDefault>.Create;
end;

procedure TfrmCadDefault.FormDestroy(Sender: TObject);
begin
  FChilds.Free;
end;

procedure TfrmCadDefault.FormShow(Sender: TObject);
begin
  DoAddChilds;
  DoRepaint;
end;

class function TfrmCadDefault.Edit<T>(const pObject: T): Boolean;
var
  lForm: TfrmCadDefault;
begin
  LForm := Self.Create(Application);
  try
    lForm.SetObject(pObject);
    Result := (lForm.ShowModal = mrOk);
  finally
    lForm.Free;
  end;
end;

function TfrmCadDefault.GetObjectAs<T>: T;
begin
  Result := T(FObject);
end;

procedure TfrmCadDefault.SetObject(pObject: TObject);
begin
  FObject := pObject;
  DoGetObjectProperties;
end;

end.
