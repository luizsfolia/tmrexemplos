unit ViewFindDefault;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.StdCtrls,
  System.Actions, Vcl.ActnList, Spring.Persistence.Core.Interfaces,
  Spring.Collections, Spring.Persistence.Core.Session, ViewCadDefault,
  Vcl.Buttons;

type

  TOnGetData<T : class, constructor> = reference to procedure(Sender: TObject; Item: TListItem; pItemObject : T);
  TOnGetConfigList = reference to procedure(Sender: TListView);

  TStateForm = (sfFind, sfPendent);

  TfrmFindDefault = class(TForm)
    lstData: TListView;
    pnlActions: TPanel;
    pnlSelect: TPanel;
    edtFind: TEdit;
    btnFind: TButton;
    btnAdd: TButton;
    MainActionList: TActionList;
    actAdd: TAction;
    actRemove: TAction;
    actSave: TAction;
    actFind: TAction;
    Button1: TButton;
    Button2: TButton;
    btnSelect: TButton;
    actSelect: TAction;
    stbStatus: TStatusBar;
    procedure lstDataData(Sender: TObject; Item: TListItem);
    procedure actFindExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actAddExecute(Sender: TObject);
    procedure lstDataDblClick(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actRemoveExecute(Sender: TObject);
    procedure MainActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private type
    TInternalOnGetData = reference to procedure(Sender: TObject; Item: TListItem);
    TInternalOnGetItem = reference to function(Sender: TObject; Item: TListItem) : TObject;
    TInternalOnNewItem = reference to function : TObject;
    TInternalOnAddItem = TProc<TObject>;
    TInternalOnSaveItens = TProc;
    TInternalOnRemoveItem = TProc<TObject>;
  private
    FConnection: IDBConnection;
    FState : TStateForm;
//    FDatabase: TSQLiteDatabase;
    FSession: TSession;
    FInternalOnGetData : TInternalOnGetData;
    FInternalOnReload : TProc;
    FInternalOnFind : TProc<string>;
    FInternalOnEditItem : TInternalOnGetItem;
    FInternalOnNewItem : TInternalOnNewItem;
    FInternalOnAddItem : TInternalOnAddItem;
    FInternalOnSaveItens : TInternalOnSaveItens;
    FInternalOnRemoveItem : TInternalOnRemoveItem;
    FInternalOnGetConfigList : TOnGetConfigList;
    FCadClass : TfrmCadDefaultClass;
    procedure SetConnection(const Value: IDBConnection);
  protected
    procedure DoRepaint<T : class, constructor>(const pList : IList<T>);
    procedure SetOnInternalFind(const pOnGetData : TProc<string>);
    procedure SetOnInternalGetData(const pOnGetData : TInternalOnGetData);
    procedure SetOnInternalGetConfigList(const pOnGetConfigList : TOnGetConfigList);
    procedure SetOnInternalEditItem(const pOnGetData : TInternalOnGetItem);
    procedure SetOnInternalNewItem(const pOnGetData : TInternalOnNewItem);
    procedure SetOnInternalAddItem(const pOnGetData : TInternalOnAddItem);
    procedure SetOnInternalSaveItens(const pOnGetData : TInternalOnSaveItens);
    procedure SetOnInternalRemoveItem(const pOnGetData : TInternalOnRemoveItem);

    procedure SetOnReload(const pOnReload : TProc);

    procedure DoAddNewItem(const pItem : TObject);
    procedure DoReload;
    procedure DoAdd;
    procedure DoEdit;
    procedure DoSave;
    procedure DoDelete;
  public
    property Connection: IDBConnection read FConnection write SetConnection;
    property CadClass: TfrmCadDefaultClass read FCadClass write FCadClass;
    property Session: TSession read FSession;

    class function Edit<T : class, constructor>(const pConnection: IDBConnection; const pOnGetConfigList : TOnGetConfigList;
      const pOnGetData : TOnGetData<T>; const pFieldForFind : string; const pCadClass : TfrmCadDefaultClass = nil; Aowner : TComponent = nil) : T;
    class function Select<T : class, constructor>(const pConnection: IDBConnection; const pOnGetConfigList : TOnGetConfigList;
      const pOnGetData : TOnGetData<T>; const pFieldForFind : string; Aowner : TComponent = nil) : T;
  end;

var
  frmFindDefault: TfrmFindDefault;

implementation

uses
  Spring.Persistence.Criteria.Criterion.SimpleExpression,
  Spring.Persistence.SQL.Types, Spring.Persistence.Criteria.Interfaces,
  Spring.Persistence.Criteria.Criterion.LikeExpression;

{$R *.dfm}

procedure TfrmFindDefault.actAddExecute(Sender: TObject);
begin
  DoAdd;
end;

procedure TfrmFindDefault.actFindExecute(Sender: TObject);
begin
  if FState = sfPendent then
    if not MessageDlg('Have records unsaved. You realy continue ?', TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      exit;
  DoReload;
end;

procedure TfrmFindDefault.actRemoveExecute(Sender: TObject);
begin
  DoDelete;
end;

procedure TfrmFindDefault.actSaveExecute(Sender: TObject);
begin
  DoSave;
end;

procedure TfrmFindDefault.DoAdd;
var
  lItem: TObject;
  lOk: Boolean;
begin
  lItem := FInternalOnNewItem;
  try
    lOk := FCadClass.Edit(lItem);
    if lOk then
      DoAddNewItem(lItem);
  finally
    if not lOk then
      lItem.Free;
  end;
end;

procedure TfrmFindDefault.DoAddNewItem(const pItem: TObject);
begin
  if Assigned(FInternalOnAddItem) then
  begin
    FInternalOnAddItem(pITem);
    FSession.SaveAll(pItem);
    FState := sfFind;
  end;
end;

procedure TfrmFindDefault.DoDelete;
var
  lITem: TObject;
begin
  lITem := nil;
  if Assigned(FInternalOnEditItem) then
    lITem := FInternalOnEditItem(lstData, lstData.Selected);
  if lITem <> nil then
  begin
    FSession.Delete(lITem);
    if Assigned(FInternalOnRemoveItem) then
      FInternalOnRemoveItem(lITem);
  end;
end;

procedure TfrmFindDefault.DoEdit;
var
  lITem: TObject;
begin
  lITem := nil;
  if Assigned(FInternalOnEditItem) then
    lITem := FInternalOnEditItem(lstData, lstData.Selected);
  if lITem <> nil then
  begin
    if FCadClass.Edit(lITem) then
    begin
      FSession.SaveAll(lITem);
      FState := sfFind;
      lstData.Invalidate;
    end;
  end;

end;

procedure TfrmFindDefault.DoReload;
begin
  if edtFind.Text <> '' then
  begin
    if Assigned(FInternalOnFind) then
      FInternalOnFind(edtFind.Text);
  end else begin
    if Assigned(FInternalOnReload) then
      FInternalOnReload;
  end;
end;

procedure TfrmFindDefault.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if FState = sfPendent then
    CanClose := MessageDlg('Have records unsaved. You realy continue ?', TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0) = mrYes
  else
    CanClose := True;
end;

procedure TfrmFindDefault.FormDestroy(Sender: TObject);
begin
  FSession.Free;
end;

procedure TfrmFindDefault.FormShow(Sender: TObject);
begin
  FState := sfFind;
  if Assigned(FInternalOnGetConfigList) then
    FInternalOnGetConfigList(lstData)
  else begin
    lstData.Columns.Add.Caption := 'Default';
  end;
end;

procedure TfrmFindDefault.DoRepaint<T>(const pList: IList<T>);
begin
  lstData.Items.BeginUpdate;
  try
    lstData.Items.Count := pList.Count;
    lstData.Invalidate;
  finally
    lstData.Items.EndUpdate;
  end;
  stbStatus.Panels[0].Text := Format('Total: %d', [pList.Count]);
end;

procedure TfrmFindDefault.DoSave;
var
  lTran: IDBTransaction;
begin
  lTran := FSession.BeginTransaction;
  if Assigned(FInternalOnSaveItens) then
    FInternalOnSaveItens;
  lTran.Commit;
  FState := sfFind;
end;

class function TfrmFindDefault.Edit<T>(const pConnection: IDBConnection; const pOnGetConfigList : TOnGetConfigList;
  const pOnGetData : TOnGetData<T>; const pFieldForFind : string; const pCadClass : TfrmCadDefaultClass; Aowner : TComponent): T;
var
  lItens: IList<T>;
  lForm: TfrmFindDefault;
begin
  lForm := Self.Create(Aowner);
  try
    lForm.Connection := pConnection;
    lForm.CadClass := pCadClass;

    lItens := TCollections.CreateObjectList<T>(True);

    lForm.SetOnInternalGetConfigList(pOnGetConfigList);
    lForm.SetOnInternalGetData(
      procedure (Sender: TObject; Item: TListItem)
      var
        lItem: T;
      begin
        lItem := lItens[Item.Index];
        if Assigned(pOnGetData) then
          pOnGetData(Sender, Item, lItem);
      end);

    lForm.SetOnInternalFind(
      procedure(pArg : string)
      var
        lCriterion: ICriterion;
      begin
        lCriterion := TLikeExpression.Create(pFieldForFind, pArg +  '%', TWhereOperator.woLike);
        lItens := lForm.Session.FindWhere<T>(lCriterion);
        lForm.DoRepaint<T>(lItens);
      end);


    lForm.SetOnReload(
      procedure
      begin
        lItens := lForm.Session.FindAll<T>;
        lForm.DoRepaint<T>(lItens);
      end);

    lForm.SetOnInternalEditItem(
      function (Sender: TObject; Item: TListItem) : TObject
      begin
        Result := lItens[Item.Index];
      end);

    lForm.SetOnInternalRemoveItem(
      procedure (Item : TObject)
      begin
        litens.Remove(Item);
        lForm.DoRepaint<T>(lItens);
      end);

    lForm.SetOnInternalNewItem(
      function : TObject
      begin
        Result := T.Create;
      end);

    lForm.SetOnInternalAddItem(
      procedure (Item : TObject)
      begin
        lItens.Add(T(Item));
        lForm.DoRepaint<T>(lItens);
      end);

    lForm.SetOnInternalSaveItens(
      procedure
      begin
        lForm.Session.SaveList<T>(lItens);
        lForm.DoRepaint<T>(lItens);
      end);


//    lForm.DoRepaint<T>(lItens);
    Result := nil;
    if lForm.ShowModal=  mrok then
      Result := nil;
  finally
    lForm.Free;
  end;

end;

procedure TfrmFindDefault.lstDataData(Sender: TObject; Item: TListItem);
begin
  if Assigned(FInternalOnGetData) then
    FInternalOnGetData(Sender, Item);
end;

procedure TfrmFindDefault.lstDataDblClick(Sender: TObject);
begin
  DoEdit;
end;

procedure TfrmFindDefault.MainActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  actRemove.Enabled := lstData.Items.Count > 0;
  actSave.Enabled := FState = sfPendent;
end;

class function TfrmFindDefault.Select<T>(const pConnection: IDBConnection;
  const pOnGetConfigList: TOnGetConfigList; const pOnGetData: TOnGetData<T>;
  const pFieldForFind: string; Aowner: TComponent): T;
begin
  Result :=
    TfrmFindDefault.Edit<T>(
      pConnection,
      pOnGetConfigList,
      pOnGetData,
      pFieldForFind,
      nil,
      Aowner);
end;

procedure TfrmFindDefault.SetConnection(const Value: IDBConnection);
begin
  FConnection := Value;
  FSession := TSession.Create(FConnection);
end;

procedure TfrmFindDefault.SetOnInternalAddItem(
  const pOnGetData: TInternalOnAddItem);
begin
  FInternalOnAddItem := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalEditItem(
  const pOnGetData: TInternalOnGetItem);
begin
  FInternalOnEditItem := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalFind(const pOnGetData: TProc<string>);
begin
  FInternalOnFind := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalGetConfigList(
  const pOnGetConfigList: TOnGetConfigList);
begin
  FInternalOnGetConfigList := pOnGetConfigList;
end;

procedure TfrmFindDefault.SetOnInternalGetData(const pOnGetData: TInternalOnGetData);
begin
  FInternalOnGetData := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalNewItem(
  const pOnGetData: TInternalOnNewItem);
begin
  FInternalOnNewItem := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalRemoveItem(
  const pOnGetData: TInternalOnRemoveItem);
begin
  FInternalOnRemoveItem := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalSaveItens(
  const pOnGetData: TInternalOnSaveItens);
begin
  FInternalOnSaveItens := pOnGetData;
end;

procedure TfrmFindDefault.SetOnReload(const pOnReload: TProc);
begin
  FInternalOnReload := pOnReload;
end;

end.
