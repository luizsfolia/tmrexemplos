unit ViewListDefault;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Spring.Collections;

type

  TOnGetData<T : class, constructor> = reference to procedure(Sender: TObject; Item: TListItem; pItemObject : T);
  TOnGetConfigList = reference to procedure(Sender: TListView);

  TfrmListDefault = class(TForm)
    lstData: TListView;
    procedure lstDataData(Sender: TObject; Item: TListItem);
    procedure FormShow(Sender: TObject);
  private type
    TInternalOnGetData = reference to procedure(Sender: TObject; Item: TListItem);
  private
    FList : IList<TObject>;
    FInternalOnGetData : TInternalOnGetData;
    FInternalOnGetConfigList : TOnGetConfigList;
  protected
    function GetListAs<T : class, constructor> : IList<T>;
    procedure SetOnInternalGetData(const pOnGetData : TInternalOnGetData);
    procedure SetOnInternalGetConfigList(const pOnGetConfigList : TOnGetConfigList);
    procedure SetList(pList : IList<TObject>);
  public
    procedure DoRepaint; virtual;
    procedure DeleteSelected;

    class function New<T : class, constructor>(pOwner : TComponent; pParent : TWinControl; pList : IList<T>; const pOnGetConfigList : TOnGetConfigList; const pOnGetData : TOnGetData<T>) : TfrmListDefault;
  end;

var
  frmListDefault: TfrmListDefault;

implementation

uses
  System.Generics.Collections;

{$R *.dfm}

{ TfrmListDefault }

procedure TfrmListDefault.DeleteSelected;
begin
//  FList.Remove(FList[lstData.Selected.Index]);
  FList.Delete(lstData.Selected.Index);
end;

procedure TfrmListDefault.DoRepaint;
begin
  lstData.Items.BeginUpdate;
  try
    lstData.Items.Count := FList.Count;
    lstData.Invalidate;
  finally
    lstData.Items.EndUpdate;
  end;
end;

function TfrmListDefault.GetListAs<T>: IList<T>;
begin
  Result := IList<T>(FList);
end;

procedure TfrmListDefault.FormShow(Sender: TObject);
begin
  if Assigned(FInternalOnGetConfigList) then
    FInternalOnGetConfigList(lstData)
  else begin
    lstData.Columns.Add.Caption := 'Default';
  end;
end;

procedure TfrmListDefault.lstDataData(Sender: TObject; Item: TListItem);
begin
  if Assigned(FInternalOnGetData) then
    FInternalOnGetData(Sender, Item);
end;

procedure TfrmListDefault.SetList(pList: IList<TObject>);
begin
  FList := pList;

//  DoGetObjectProperties;
end;

procedure TfrmListDefault.SetOnInternalGetConfigList(const pOnGetConfigList: TOnGetConfigList);
begin
  FInternalOnGetConfigList := pOnGetConfigList;
end;

procedure TfrmListDefault.SetOnInternalGetData(const pOnGetData: TInternalOnGetData);
begin
  FInternalOnGetData := pOnGetData;
end;

class function TfrmListDefault.New<T>(pOwner: TComponent; pParent : TWinControl; pList : IList<T>; const pOnGetConfigList : TOnGetConfigList; const pOnGetData : TOnGetData<T>): TfrmListDefault;
begin
  Result := TfrmListDefault.Create(pOwner);
  Result.Parent := pParent;
  Result.Align := alClient;
  Result.SetList(IList<TObject>(pList));

  Result.SetOnInternalGetConfigList(pOnGetConfigList);

  Result.SetOnInternalGetData(
      procedure (Sender: TObject; Item: TListItem)
      var
        lItem: TObject;
      begin
        if pList.Count > Item.Index then
        begin
          lItem := pList[Item.Index];
          if Assigned(pOnGetData) then
            pOnGetData(Sender, Item, lItem);
        end;
      end);

  Result.Show;

//  FChilds.Add(pKey, lChild);
end;

end.
