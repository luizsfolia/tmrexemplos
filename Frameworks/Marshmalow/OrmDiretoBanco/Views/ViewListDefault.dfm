object frmListDefault: TfrmListDefault
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'List Default'
  ClientHeight = 503
  ClientWidth = 671
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnShow = FormShow
  TextHeight = 15
  object lstData: TListView
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 665
    Height = 497
    Align = alClient
    Columns = <>
    GridLines = True
    OwnerData = True
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnData = lstDataData
    ExplicitLeft = -46
    ExplicitTop = 92
    ExplicitWidth = 701
    ExplicitHeight = 222
  end
end
