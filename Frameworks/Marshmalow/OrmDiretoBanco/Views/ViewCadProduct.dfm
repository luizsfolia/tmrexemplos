inherited frmCadProduct: TfrmCadProduct
  Caption = 'frmCadProduct'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlData: TPanel
    object NameLabel: TLabel
      Left = 16
      Top = 35
      Width = 31
      Height = 13
      Caption = 'Name:'
    end
    object PriceLabel: TLabel
      Left = 16
      Top = 62
      Width = 27
      Height = 13
      Caption = 'Price:'
    end
    object QuantityLabel: TLabel
      Left = 16
      Top = 89
      Width = 46
      Height = 13
      Caption = 'Quantity:'
    end
    object NameEdit: TEdit
      Left = 80
      Top = 32
      Width = 309
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      TextHint = 'Name'
    end
    object PriceMaskEdit: TMaskEdit
      Left = 80
      Top = 59
      Width = 309
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
      Text = ''
      TextHint = 'Price'
    end
    object QuantitySpinEdit: TSpinEdit
      Left = 80
      Top = 86
      Width = 309
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 2
      Value = 1
    end
  end
end
