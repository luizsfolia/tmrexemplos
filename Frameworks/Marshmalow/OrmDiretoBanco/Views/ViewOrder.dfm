inherited frmOrder: TfrmOrder
  Caption = 'Order'
  ClientHeight = 597
  ClientWidth = 705
  ExplicitWidth = 717
  ExplicitHeight = 635
  TextHeight = 13
  inherited pnlActions: TPanel
    Top = 518
    Width = 705
    ExplicitTop = 517
    ExplicitWidth = 701
  end
  inherited pnlData: TPanel
    Width = 705
    Height = 518
    ExplicitWidth = 701
    ExplicitHeight = 517
    object QuantityLabel: TLabel
      Left = 16
      Top = 41
      Width = 41
      Height = 13
      Caption = 'Number:'
    end
    object PriceLabel: TLabel
      Left = 16
      Top = 69
      Width = 31
      Height = 13
      Caption = 'Client:'
    end
    object Label1: TLabel
      Left = 16
      Top = 133
      Width = 26
      Height = 13
      Caption = 'Item:'
    end
    object Label2: TLabel
      Left = 18
      Top = 161
      Width = 56
      Height = 13
      Caption = 'Quantidade'
    end
    object Label3: TLabel
      Left = 18
      Top = 189
      Width = 27
      Height = 13
      Caption = 'Pre'#231'o'
    end
    object NumberSpinEdit: TSpinEdit
      Left = 80
      Top = 38
      Width = 73
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 0
      Value = 1
      ExplicitWidth = 69
    end
    object ClientName: TMaskEdit
      Left = 174
      Top = 66
      Width = 301
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
      Text = ''
      TextHint = 'Price'
      ExplicitWidth = 297
    end
    object speClientId: TSpinEdit
      Left = 80
      Top = 66
      Width = 73
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 2
      Value = 1
      ExplicitWidth = 69
    end
    object speItem: TSpinEdit
      Left = 80
      Top = 130
      Width = 73
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 3
      Value = 1
      ExplicitWidth = 69
    end
    object edtITem: TMaskEdit
      Left = 174
      Top = 130
      Width = 301
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 4
      Text = ''
      TextHint = 'Price'
      ExplicitWidth = 297
    end
    object speQuantidade: TSpinEdit
      Left = 80
      Top = 158
      Width = 73
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 5
      Value = 1
      ExplicitWidth = 69
    end
    object Button1: TButton
      Left = 46
      Top = 249
      Width = 75
      Height = 25
      Action = actAddItem
      TabOrder = 6
    end
    object Button2: TButton
      Left = 489
      Top = 64
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 7
    end
    object Button3: TButton
      Left = 127
      Top = 250
      Width = 90
      Height = 25
      Action = actRemoveItem
      TabOrder = 8
    end
    object spePrecoProduto: TSpinEdit
      Left = 80
      Top = 186
      Width = 73
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      MaxValue = 0
      MinValue = 0
      TabOrder = 9
      Value = 1
      ExplicitWidth = 69
    end
    object Panel1: TPanel
      Left = 24
      Top = 312
      Width = 649
      Height = 193
      Caption = 'Panel1'
      TabOrder = 10
    end
  end
  inherited MainActionList: TActionList
    object actAddItem: TAction
      Caption = 'Add Item'
      OnExecute = actAddItemExecute
    end
    object actRemoveItem: TAction
      Caption = 'Remover Item'
      OnExecute = actRemoveItemExecute
    end
  end
end
