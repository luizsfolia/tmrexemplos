object frmCadDefault: TfrmCadDefault
  Left = 0
  Top = 0
  Caption = 'frmCadDefault'
  ClientHeight = 367
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 13
  object pnlActions: TPanel
    Left = 0
    Top = 288
    Width = 617
    Height = 79
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'pnlActions'
    ShowCaption = False
    TabOrder = 0
    ExplicitTop = 287
    ExplicitWidth = 613
    object btnOk: TButton
      Left = 489
      Top = 24
      Width = 105
      Height = 33
      Action = actOk
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 378
      Top = 24
      Width = 105
      Height = 33
      Action = actCancel
      TabOrder = 1
    end
  end
  object pnlData: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 288
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnlData'
    ShowCaption = False
    TabOrder = 1
    ExplicitWidth = 613
    ExplicitHeight = 287
  end
  object MainActionList: TActionList
    Left = 448
    Top = 176
    object actOk: TAction
      Caption = 'Save'
      OnExecute = actOkExecute
    end
    object actCancel: TAction
      Caption = 'Cancel'
      OnExecute = actCancelExecute
    end
  end
end
