unit ViewOrder;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ViewCadDefault, System.Actions,
  Vcl.ActnList, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Samples.Spin, Vcl.Mask,
  Vcl.ComCtrls, OrderItemModel, Spring.Collections;

type
  TfrmOrder = class(TfrmCadDefault)
    QuantityLabel: TLabel;
    NumberSpinEdit: TSpinEdit;
    ClientName: TMaskEdit;
    PriceLabel: TLabel;
    speClientId: TSpinEdit;
    Label1: TLabel;
    speItem: TSpinEdit;
    edtITem: TMaskEdit;
    speQuantidade: TSpinEdit;
    Label2: TLabel;
    Button1: TButton;
    actAddItem: TAction;
    Button2: TButton;
    Button3: TButton;
    actRemoveItem: TAction;
    spePrecoProduto: TSpinEdit;
    Label3: TLabel;
    Panel1: TPanel;
    procedure actAddItemExecute(Sender: TObject);
    procedure actRemoveItemExecute(Sender: TObject);
  private
    FItens : IList<TOrderItem>;
  protected
    procedure DoGetObjectProperties; override;
    procedure DoSetObjectProperties; override;
    procedure DoAddChilds; override;
  public
    { Public declarations }
  end;

var
  frmOrder: TfrmOrder;

implementation

{$R *.dfm}

uses OrderModel, ClientModel, ProductModel, ViewListDefault;

procedure TfrmOrder.actAddItemExecute(Sender: TObject);
var
  lItem: TOrderItem;
begin
  inherited;

  lItem := TOrderItem.Create;
  lItem.ProductId := speItem.Value;
  lItem.Price := spePrecoProduto.Value;
  lItem.Quantity := speQuantidade.Value;

  GetObjectAs<TOrder>.Itens.Add(lItem);

  DoRepaint;
//
end;

procedure TfrmOrder.actRemoveItemExecute(Sender: TObject);
begin
  inherited;
  DoRemoveChildSelected('Itens');
end;

procedure TfrmOrder.DoAddChilds;
begin
  inherited;
  DoAddChild('Itens',
    TfrmListDefault.New<TOrderItem>(
      Self,
      Panel1,
      GetObjectAs<TOrder>.Itens,
      procedure(Sender: TListView)
      begin
        Sender.Columns.Add.Caption := 'ID';
        Sender.Columns.Add.Caption := 'ProductId';
        Sender.Columns.Add.Caption := 'Price';
        Sender.Columns.Add.Caption := 'Quantity';
      end,
      procedure (Sender: TObject; Item: TListItem; pItemObject : TOrderItem)
      begin
        Item.Caption := pItemObject.ID.ToString;
        Item.SubItems.Add(pItemObject.ProductId.ToString);
        Item.SubItems.Add(pItemObject.Quantity.ToString);
        Item.SubItems.Add(pItemObject.Price.ToString);
      end)
  );
end;

procedure TfrmOrder.DoGetObjectProperties;
begin
  inherited;
  NumberSpinEdit.Value := GetObjectAs<TOrder>.Number;
  speClientId.Value := GetObjectAs<TOrder>.Client.ID;
  ClientName.Text := GetObjectAs<TOrder>.Client.Name;
end;

procedure TfrmOrder.DoSetObjectProperties;
begin
  inherited;
  GetObjectAs<TOrder>.Number := NumberSpinEdit.Value;
  GetObjectAs<TOrder>.ClientId := speClientId.Value;
end;

end.
