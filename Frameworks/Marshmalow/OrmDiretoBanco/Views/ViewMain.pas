unit ViewMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Spring.Persistence.Core.Interfaces, Spring.Persistence.Core.Session,
  System.Actions, Vcl.ActnList, System.Classes, OrderModel, Spring.Collections,
  Vcl.Buttons, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef;

type

  TMySession = class(TSession)
  public
    function GetMyOrder: IList<TOrder>;
  end;

  TfrmMain = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    MainActionList: TActionList;
    RebuildDatabaseAction: TAction;
    actProducts: TAction;
    actCadClient: TAction;
    actOrder: TAction;
    Button5: TButton;
    Memo1: TMemo;
    BitBtn1: TBitBtn;
    FDConnection1: TFDConnection;
    procedure FormCreate(Sender: TObject);
    procedure RebuildDatabaseActionExecute(Sender: TObject);
    procedure actProductsExecute(Sender: TObject);
    procedure actCadClientExecute(Sender: TObject);
    procedure actOrderExecute(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    FConnection: IDBConnection;
    FSession: TMySession;
    FFDConnection: TFDConnection;

    procedure DoBuildDatabase;
    procedure DoCheckEntities;
  public
    property Connection: IDBConnection read FConnection;
    property Session: TMySession read FSession;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  Spring.Persistence.Core.DatabaseManager, SQLiteTable3,
  Spring.Persistence.Adapters.SQLite, ProductModel,
  ViewFindDefault, Vcl.ComCtrls, ViewCadProduct, ClientModel, ViewCadClient, ViewOrder, Spring.Persistence.SQL.Commands.Select,
  OrderItemModel, Spring.Persistence.Criteria.Interfaces,
  Spring.Persistence.Criteria.Criterion.SimpleExpression,
  Spring.Persistence.SQL.Types, Spring.Persistence.SQL.Commands,
  Spring.Persistence.SQL.Generators.Ansi, Spring.Persistence.SQL.Generators.Oracle,
  Spring.Persistence.Adapters.FireDac,
  Spring.Persistence.SQL.Interfaces;

{$R *.dfm}

procedure TfrmMain.actCadClientExecute(Sender: TObject);
begin
  TfrmFindDefault.Edit<TClient>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'ID';
      Sender.Columns.Add.Caption := 'Name';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TClient)
    begin
      Item.Caption := pItemObject.ID.ToString;
      Item.SubItems.Add(pItemObject.Name);
    end,
    'CLINAME',
    TfrmCadClient,
    Self);
end;

procedure TfrmMain.actOrderExecute(Sender: TObject);
begin
  TfrmFindDefault.Edit<TOrder>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'ID';
      Sender.Columns.Add.Caption := 'Date';
      Sender.Columns.Add.Caption := 'Number';
      Sender.Columns.Add.Caption := 'Total';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TOrder)
    begin
      Item.Caption := pItemObject.ID.ToString;
      Item.SubItems.Add(DateToStr(pItemObject.Date));
      Item.SubItems.Add(pItemObject.Number.ToString);
//      Item.SubItems.Add(IntToStr(pItemObject.Quantity));
    end,
    'ORDERNUMBER',
    TfrmOrder,
    Self);
end;

procedure TfrmMain.actProductsExecute(Sender: TObject);
begin
  TfrmFindDefault.Edit<TProduct>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'Name';
      Sender.Columns.Add.Caption := 'Price';
      Sender.Columns.Add.Caption := 'Quantity';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TProduct)
    begin
      Item.Caption := pItemObject.Name;
      Item.SubItems.Add(CurrToStr(pItemObject.Price));
      Item.SubItems.Add(IntToStr(pItemObject.Quantity));
    end,
    'PRODNAME',
    TfrmCadProduct,
    Self);
end;

procedure TfrmMain.BitBtn1Click(Sender: TObject);
var
  sSql: string;
  LCommand: TSelectCommand;
  LTable, LJoinTable, LCountriesTable: TSQLTable;
  LJoin: TSQLJoin;
  FSQLGenerator: ISQLGenerator;
begin

  LTable := TSQLTable.Create;
  LTable.Schema := 'TEST';
  LTable.Name := 'CUSTOMERS';
  LTable.Description := 'Customers table';

//  LTable := CreateTestTable;

  LJoinTable := TSQLTable.Create(1);
  LJoinTable.Schema := 'TEST';
  LJoinTable.Name := 'PRODUCTS';
  LJoinTable.Description := 'Products table';

//  LCountriesTable := CreateTestCOUNTRYTable;

  LCountriesTable := TSQLTable.Create(2);
  LCountriesTable.Schema := 'TEST';
  LCountriesTable.Name := 'COUNTRIES';
  LCountriesTable.Description := 'Countries table';

  FSQLGenerator := TOracleSQLGenerator.Create;

  LCommand := TSelectCommand.Create(LTable);
  try
    LCommand.SelectFields.Add(TSQLSelectField.Create('NAME', LTable));
    LCommand.SelectFields.Add(TSQLSelectField.Create('AGE', LTable));
    LCommand.SelectFields.Add(TSQLSelectField.Create('HEIGHT', LTable));

    sSql := FSQLGenerator.GenerateSelect(LCommand);

    Memo1.Lines.Add('SQL : ' + sLineBreak + sSql + sLineBreak);

    LJoin := TSQLJoin.Create(jtInner);
    LJoin.Segments.Add(
      TSQLJoinSegment.Create(
        TSQLField.Create('ID', LJoinTable),
        TSQLField.Create('PRODID', LTable)));
    LCommand.Joins.Add(LJoin);

    sSql := FSQLGenerator.GenerateSelect(LCommand);

    Memo1.Lines.Add('SQL : ' + sLineBreak + sSql + sLineBreak);

    LCommand.SelectFields.Add(TSQLSelectField.Create('COUNTRYNAME', LCountriesTable));
    LJoin := TSQLJoin.Create(jtLeft);
    LJoin.Segments.Add(
      TSQLJoinSegment.Create(
        TSQLField.Create('ID', LCountriesTable),
        TSQLField.Create('COUNTRYID', LTable)));
    LCommand.Joins.Add(LJoin);

    sSql := FSQLGenerator.GenerateSelect(LCommand);
    Memo1.Lines.Add('SQL : ' + sLineBreak + sSql + sLineBreak);

    LCommand.OrderByFields.Add(TSQLOrderByField.Create('AGE', LTable, stDescending));

    sSql := FSQLGenerator.GenerateSelect(LCommand);
    Memo1.Lines.Add('SQL : ' + sLineBreak + sSql + sLineBreak);

    LCommand.OrderByFields.Add(TSQLOrderByField.Create('COUNTRYNAME', LCountriesTable, stAscending));
    sSql := FSQLGenerator.GenerateSelect(LCommand);
    Memo1.Lines.Add('SQL : ' + sLineBreak + sSql + sLineBreak);

    LCommand.GroupByFields.Add(TSQLGroupByField.Create('HEIGHT', LTable));
    LCommand.GroupByFields.Add(TSQLGroupByField.Create('NAME', LTable));
    LCommand.GroupByFields.Add(TSQLGroupByField.Create('AGE', LTable));
    LCommand.GroupByFields.Add(TSQLGroupByField.Create('COUNTRYNAME', LCountriesTable));

    sSql := FSQLGenerator.GenerateSelect(LCommand);
    Memo1.Lines.Add('SQL : ' + sLineBreak + sSql + sLineBreak);

    LCommand.WhereFields.Add(TSQLWhereField.Create('NAME', LTable));

    sSql := FSQLGenerator.GenerateSelect(LCommand);
    Memo1.Lines.Add('SQL : ' + sLineBreak + sSql + sLineBreak);




  finally
    LTable.Free;
    LJoinTable.Free;
    LCountriesTable.Free;
    LCommand.Free;
  end;

end;

procedure TfrmMain.Button5Click(Sender: TObject);
var
  lItens: IList<TOrder>;
  lITem: TOrder;
  lITemOrder: TOrderItem;
  lOrder: TOrder;
begin

  lOrder := TOrder.Create;

  lOrder.ClientId := 1;
  lOrder.Number := 1234;

  lITemOrder := TOrderItem.Create;
  lITemOrder.ProductId := 1;
  lITemOrder.Price := 10;
  lITemOrder.Quantity := 10;

  lOrder.Itens.Add(lITemOrder);

  lITemOrder := TOrderItem.Create;
  lITemOrder.ProductId := 1;
  lITemOrder.Price := 10;
  lITemOrder.Quantity := 10;

  lOrder.Itens.Add(lITemOrder);

  FSession.SaveAll(lOrder);

  lItens := FSession.GetMyOrder;

  for lITem in lItens do
  begin
    Memo1.Lines.Add(lITem.ID.ToString);
    for lITemOrder in lITem.Itens do
    begin
      Memo1.Lines.Add('>' + lITemOrder.ID.ToString + ' - ' + lITemOrder.Product.Name);
    end;
  end;
end;

procedure TfrmMain.DoBuildDatabase;
var
  lDBManager: TDatabaseManager;
begin
  lDBManager := TDatabaseManager.Create(FConnection);
  try
    lDBManager.BuildDatabase;
  finally
    lDBManager.Free;
  end;
end;

procedure TfrmMain.DoCheckEntities;
var
  lDBManager: TDatabaseManager;
begin
  lDBManager := TDatabaseManager.Create(FConnection);
  try
    if (not lDBManager.EntityExists(TProduct)) then
      lDBManager.BuildDatabase;
  finally
    lDBManager.Free;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  FDatabase: TSQLiteDatabase;
begin
  FDatabase := TSQLiteDatabase.Create(Self);
  FDatabase.Filename := 'products.db3';
  FConnection := TSQLiteConnectionAdapter.Create(FDatabase);


//  FFDConnection := TFDConnection.Create(Self);

//object FDConnection1: TFDConnection
//  Params.Strings = (
//  FFDConnection.Filename := 'products.db3';
//  FConnection := TSQLiteConnectionAdapter.Create(FDatabase);


  FConnection.AutoFreeConnection := True;
  FConnection.Connect;
  FSession := TMySession.Create(FConnection);




  DoCheckEntities;
end;

procedure TfrmMain.RebuildDatabaseActionExecute(Sender: TObject);
begin
  DoBuildDatabase;
end;

{ TMySession }

function TMySession.GetMyOrder: IList<TOrder>;
begin
  Result := Self.FindAll<TOrder>;
end;

end.
