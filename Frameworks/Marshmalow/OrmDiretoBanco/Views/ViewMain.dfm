object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Main'
  ClientHeight = 358
  ClientWidth = 637
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  DesignSize = (
    637
    358)
  TextHeight = 13
  object Button1: TButton
    Left = 24
    Top = 96
    Width = 105
    Height = 49
    Action = actProducts
    TabOrder = 0
  end
  object Button2: TButton
    Left = 24
    Top = 24
    Width = 105
    Height = 49
    Action = RebuildDatabaseAction
    TabOrder = 1
  end
  object Button3: TButton
    Left = 24
    Top = 168
    Width = 105
    Height = 49
    Action = actCadClient
    TabOrder = 2
  end
  object Button4: TButton
    Left = 24
    Top = 232
    Width = 105
    Height = 49
    Action = actOrder
    TabOrder = 3
  end
  object Button5: TButton
    Left = 177
    Top = 17
    Width = 217
    Height = 73
    Caption = 'Exemplo Cria Order e adiciona itens'
    TabOrder = 4
    OnClick = Button5Click
  end
  object Memo1: TMemo
    Left = 160
    Top = 96
    Width = 455
    Height = 254
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo1')
    TabOrder = 5
    ExplicitWidth = 451
    ExplicitHeight = 253
  end
  object BitBtn1: TBitBtn
    Left = 416
    Top = 17
    Width = 199
    Height = 73
    Caption = 'Inner Exemplo'
    TabOrder = 6
    OnClick = BitBtn1Click
  end
  object MainActionList: TActionList
    Left = 408
    Top = 200
    object RebuildDatabaseAction: TAction
      Category = 'Database'
      Caption = 'Rebuild Database'
      OnExecute = RebuildDatabaseActionExecute
    end
    object actProducts: TAction
      Category = 'Cads'
      Caption = 'Cad Products'
      OnExecute = actProductsExecute
    end
    object actCadClient: TAction
      Category = 'Cads'
      Caption = 'Cad Client'
      OnExecute = actCadClientExecute
    end
    object actOrder: TAction
      Category = 'Cads'
      Caption = 'Order'
      OnExecute = actOrderExecute
    end
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Server=dev\dev_barretos'
      'Database=dbacesso'
      'User_Name=minerva'
      'Password=m1N3rv4_&_M1n3RV4'
      'DriverID=MSSQL')
    LoginPrompt = False
    Left = 312
    Top = 184
  end
end
