unit uSession;

interface

uses
  Spring.Persistence.Core.Session, Spring.Collections;


type

  TSystemSession = class(TSession)
  public
    function GetMyOrder: IList<TOrder>;
  end;


implementation

end.
