unit Registry;

interface

uses
  OrderItemRepository;

procedure RegisterTypes;

implementation

uses
  Spring.Persistence.Core.Interfaces, OrderItemModel, Spring.Container;

procedure RegisterTypes;
var
  container: TContainer;
begin
  container := GlobalContainer;

  container.RegisterType<IPagedRepository<TOrderItem,Integer>, TOrderItemRepository>;

//  container.RegisterType<TObject, TCustomersViewModel>('customersViewModel').AsSingleton;
//
//  container.RegisterType<TCustomersView, TCustomersView>.DelegateTo(
//    function: TCustomersView
//    begin
//      Application.CreateForm(TCustomersView, Result);
//    end);

  container.Build;
end;



end.
