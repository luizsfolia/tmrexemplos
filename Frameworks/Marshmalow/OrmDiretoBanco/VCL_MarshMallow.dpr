program VCL_MarshMallow;

uses
  Forms,
  Spring.Persistence.Adapters.SQLite,
  ProductModel in 'Source\ProductModel.pas',
  OrderModel in 'Source\OrderModel.pas',
  OrderItemModel in 'Source\OrderItemModel.pas',
  ClientModel in 'Source\ClientModel.pas',
  ViewFindDefault in 'Views\ViewFindDefault.pas' {frmFindDefault},
  ViewCadDefault in 'Views\ViewCadDefault.pas' {frmCadDefault},
  ViewCadProduct in 'Views\ViewCadProduct.pas' {frmCadProduct},
  ViewMain in 'Views\ViewMain.pas' {frmMain},
  ViewCadClient in 'Views\ViewCadClient.pas' {frmCadClient},
  ViewOrder in 'Views\ViewOrder.pas' {frmOrder},
  OrderItemRepository in 'Source\OrderItemRepository.pas',
  Registry in 'Source\Registry.pas',
  ViewListDefault in 'Views\ViewListDefault.pas' {frmListDefault};

{$R *.res}

begin
  RegisterTypes;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmListDefault, frmListDefault);
  {$WARNINGS OFF}
//  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
{$WARNINGS ON}
  Application.Run;
end.
