unit uPrincipalServerHorseMarshmallow;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Horse, SQLiteTable3,
  Spring.Persistence.Adapters.SQLite, uSession, Spring.Persistence.Core.DatabaseManager,
  ProductModel, ClientModel, OrderModel, OrderItemModel, uConnection,
  Spring.Persistence.Core.Interfaces, uClients, uProducts, uOrders;

type
  TForm2 = class(TForm)
    btnStart: TBitBtn;
    BitBtn2: TBitBtn;
    btnStop: TBitBtn;

    procedure btnStartClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
  private
//    FConnection : TSQLiteConnectionAdapter;
//    FSession : TSystemSession;
    procedure DoCheckEntities(const pConnection : IDBConnection);
    procedure DoGetObject;
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses
  Rest.Json, System.JSON, Spring.Collections.MultiMaps, Spring.Collections,
  Horse.JWT, JOSE.Core.JWT, JOSE.Core.Builder, System.DateUtils;

{$R *.dfm}

procedure TForm2.btnStartClick(Sender: TObject);
var
  lConnection: TSystemConnection;
begin
//  THorse.Use(HorseJWT('12345', THorseJWTConfig.New.SkipRoutes(['create-token'])));
//
//  THorse.Post('create-token',
//    procedure(Req: THorseRequest; Res: THorseResponse; Next: TProc)
//    var
//      LJWT: TJWT;
//      LToken: String;
//    begin
//      LJWT := TJWT.Create();
//      try
//        // Enter the payload data
//        LJWT.Claims.Expiration := IncHour(Now, 1);
//        LJWT.Claims.SetClaimOfType<string>('Teste', '12345');
//
//        // Generating the token
//        LToken := TJOSE.SHA256CompactToken('12345', LJWT);
//      finally
//        FreeAndNil(LJWT);
//      end;
//
//      // Sending the token
//      Res.Send(LToken);
//    end);
//

    THorse.Get('/ping',
      procedure(Req: THorseRequest; Res: THorseResponse; Next: TProc)
      begin
        Res.Send('pong');
      end);

    THorse.Get('/Clients', GetClients);
    THorse.Get('/Clients/:id', GetClient);
    THorse.Put('/Clients/:id', SetClient);
    THorse.Post('/Clients', SetClient);
    THorse.Delete('/Clients/:id', DelClient);
    THorse.Get('/Clients/Buscar', FindClient);

    THorse.Get('/Products', GetProducts);
    THorse.Get('/Products/:id', GetProduct);
    THorse.Put('/Products/:id', SetProduct);
    THorse.Post('/Products', SetProduct);
    THorse.Delete('/Products/:id', DelProduct);
    THorse.Get('/Products/Buscar', FindProduct);


    THorse.Get('/Orders', GetOrders);
    THorse.Get('/Orders/:id', GetOrder);
    THorse.Put('/Orders/:id', SetOrder);
    THorse.Post('/Orders', SetOrder);
    THorse.Delete('/Orders/:id', DelOrder);
    THorse.Get('/Orders/Buscar', FindOrder);

    THorse.Get('/OrderItens', GetOrders);
    THorse.Get('/OrderItens/:id', GetOrder);
    THorse.Put('/OrderItens/:id', SetOrder);
    THorse.Post('/OrderItens', SetOrder);
    THorse.Delete('/OrderItens/:id', DelOrder);
    THorse.Get('/OrderItens/Buscar', FindOrder);

    THorse.Listen(8500,
    procedure(horse : THorse)
    begin
      btnStart.Enabled := False;
      btnStop.Enabled := True;
    end,
    procedure(horse : THorse)
    begin
      btnStart.Enabled := True;
      btnStop.Enabled := False;
    end);
end;

procedure TForm2.btnStopClick(Sender: TObject);
begin
  THorse.StopListen;
end;

procedure TForm2.BitBtn2Click(Sender: TObject);
var
  lConnection: TSystemConnection;
begin
  lConnection := TSystemConnection.GetConnection;
  try
    DoCheckEntities(lConnection.Connection);
    ShowMessage('DataBase Criado com sucesso')
  finally
    lConnection.Free;
  end;
end;

procedure TForm2.DoCheckEntities(const pConnection : IDBConnection);
var
  lDBManager: TDatabaseManager;
begin
  lDBManager := TDatabaseManager.Create(pConnection);
  try
    if (not lDBManager.EntityExists(TProduct)) or
       (not lDBManager.EntityExists(TClient)) or
       (not lDBManager.EntityExists(TOrder)) or
       (not lDBManager.EntityExists(TOrderItem)) then
      lDBManager.BuildDatabase;
  finally
    lDBManager.Free;
  end;

end;

procedure TForm2.DoGetObject;
//var
//  lItens: IList<T>;
////  lForm: TfrmFindDefault;
begin
////  lForm := Self.Create(Aowner);
////  try
////    lForm.Connection := pConnection;
////    lForm.CadClass := pCadClass;
//
//    lItens := TCollections.CreateObjectList<TClient>(True);
//
////    lForm.SetOnInternalGetConfigList(pOnGetConfigList);
////    lForm.SetOnInternalGetData(
////      procedure (Sender: TObject; Item: TListItem)
////      var
////        lItem: T;
////      begin
////        lItem := lItens[Item.Index];
////        if Assigned(pOnGetData) then
////          pOnGetData(Sender, Item, lItem);
////      end);
////
//    lForm.SetOnInternalFind(
//      procedure(pArg : string)
//      var
//        lCriterion: ICriterion;
//      begin
//        lCriterion := TLikeExpression.Create(pFieldForFind, pArg +  '%', TWhereOperator.woLike);
//        lItens := lForm.Session.FindWhere<T>(lCriterion);
//        lForm.DoRepaint<T>(lItens);
//      end);
//
//
//    lForm.SetOnReload(
//      procedure
//      begin
//        lItens := lForm.Session.FindAll<T>;
//        lForm.DoRepaint<T>(lItens);
//      end);
//
//    lForm.SetOnInternalEditItem(
//      function (Sender: TObject; Item: TListItem) : TObject
//      begin
//        Result := lItens[Item.Index];
//      end);
//
//    lForm.SetOnInternalRemoveItem(
//      procedure (Item : TObject)
//      begin
//        litens.Remove(Item);
//        lForm.DoRepaint<T>(lItens);
//      end);
//
//    lForm.SetOnInternalNewItem(
//      function : TObject
//      begin
//        Result := T.Create;
//      end);
//
//    lForm.SetOnInternalAddItem(
//      procedure (Item : TObject)
//      begin
//        lItens.Add(T(Item));
//        lForm.DoRepaint<T>(lItens);
//      end);
//
//    lForm.SetOnInternalSaveItens(
//      procedure
//      begin
//        lForm.Session.SaveList<T>(lItens);
//        lForm.DoRepaint<T>(lItens);
//      end);
//
//
////    lForm.DoRepaint<T>(lItens);
//    Result := nil;
//    if lForm.ShowModal=  mrok then
//      Result := nil;
//  finally
//    lForm.Free;
//  end;
end;

end.
