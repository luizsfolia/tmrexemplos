unit uProducts;

interface

uses
  Horse, Spring.Persistence.Criteria.Interfaces, uConnection, uSession,
  Spring.Persistence.Criteria.Criterion.LikeExpression, ProductModel,
  Spring.Collections, Spring.Persistence.SQL.Types;

procedure GetProducts(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure GetProduct(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure SetProduct(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure DelProduct(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure FindProduct(Req: THorseRequest; Res: THorseResponse; Next: TProc);


implementation

uses
  REST.Json, System.JSON;

procedure GetProducts(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lProducts: IList<TProduct>;
  lProduct: TProduct;
  lJson: TJSONArray;
  lConnection: TSystemConnection;
  lPageIndex: Integer;
  lSize: Integer;
  lFind: string;
  lCriterion: ICriterion;
begin
  Res.ContentType('application/json');

  lPageIndex := 0;
  lSize := 0;
  lFind := '';

  Req.Query.TryGetValue('pFiltro',lFind);
  if Req.Query.ContainsKey('page_index') then
    lPageIndex := Req.Query.Field('page_index').AsInteger;
  if Req.Query.ContainsKey('size') then
    lSize := Req.Query.Field('size').AsInteger;
  if Req.Query.ContainsKey('find') then
    lFind := Req.Query.Field('find').Asstring;

  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);

    if lFind <> '' then
    begin
      lCriterion := TLikeExpression.Create('PRODNAME', lFind +  '%', TWhereOperator.woLike);
      lProducts := lSession.FindWhere<TProduct>(lCriterion);
    end else begin
      if (lPageIndex > 0) and (lSize > 0) then
        lProducts := lSession.Page<TProduct>(lPageIndex, lSize).Items
      else
        lProducts := lSession.FindAll<TProduct>;
    end;

    lJson := TJSONArray.Create;
    try
      for lProduct in lProducts.ToArray do
        lJson.AddElement(TJson.ObjectToJsonObject(lProduct));
      Res.Send(lJson.ToString);
    finally
      lJson.Free;
    end;
  finally
    lConnection.Free;
  end;
end;

procedure GetProduct(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lProducts: IList<TProduct>;
  lProduct: TProduct;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lId := req.Params.Field('id').AsInteger;
    lProduct := lSession.GetProduct(lid);
    Res.Send(TJson.ObjectToJsonString(lProduct));
  finally
    lConnection.Free;
  end;
end;

procedure SetProduct(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lProducts: IList<TProduct>;
  lProduct: TProduct;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lProduct := TJson.JsonToObject<TProduct>(req.Body);
    try
      lId := lSession.SetProduct(lProduct);
      Res.Send(TJson.ObjectToJsonString(lProduct));
    finally
      lProduct.Free;
    end;
  finally
    lConnection.Free;
  end;
end;

procedure DelProduct(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lProducts: IList<TProduct>;
  lProdct: TProduct;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lId := req.Params.Field('id').AsInteger;
    lSession.DelClient(lId);
  finally
    lConnection.Free;
  end;
end;

procedure FindProduct(Req: THorseRequest; Res: THorseResponse; Next: TProc);
begin
  GetProducts(Req, Res, Next);
end;



end.
