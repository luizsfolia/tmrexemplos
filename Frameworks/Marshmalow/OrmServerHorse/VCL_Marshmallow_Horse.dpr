program VCL_Marshmallow_Horse;

uses
  Vcl.Forms,
  uPrincipalServerHorseMarshmallow in 'uPrincipalServerHorseMarshmallow.pas' {Form2},
  OrderItemRepository in 'Source\OrderItemRepository.pas',
  OrderModel in 'Source\OrderModel.pas',
  ProductModel in 'Source\ProductModel.pas',
  Registry in 'Source\Registry.pas',
  ClientModel in 'Source\ClientModel.pas',
  OrderItemModel in 'Source\OrderItemModel.pas',
  uSession in 'Source\uSession.pas',
  uConnection in 'uConnection.pas',
  uClients in 'uClients.pas',
  uProducts in 'uProducts.pas',
  uOrders in 'uOrders.pas',
  uOrderItem in 'uOrderItem.pas';

{$R *.res}

begin
//  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
