unit uOrderItem;

interface

uses
  Horse, Spring.Persistence.Criteria.Interfaces, uConnection, uSession,
  Spring.Persistence.Criteria.Criterion.LikeExpression, ProductModel,
  Spring.Collections, Spring.Persistence.SQL.Types, OrderItemModel;

procedure GetOrderItens(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure GetOrderItem(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure SetOrderItem(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure DelOrderItem(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure FindOrderItem(Req: THorseRequest; Res: THorseResponse; Next: TProc);


implementation

uses
  REST.Json, System.JSON;

procedure GetOrderItens(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lOrderItens: IList<TOrderItem>;
  lOrderItem: TOrderItem;
  lJson: TJSONArray;
  lConnection: TSystemConnection;
  lPageIndex: Integer;
  lSize: Integer;
  lFind: string;
  lCriterion: ICriterion;
begin
  Res.ContentType('application/json');

  lPageIndex := 0;
  lSize := 0;
  lFind := '';

  Req.Query.TryGetValue('pFiltro',lFind);
  if Req.Query.ContainsKey('page_index') then
    lPageIndex := Req.Query.Field('page_index').AsInteger;
  if Req.Query.ContainsKey('size') then
    lSize := Req.Query.Field('size').AsInteger;
  if Req.Query.ContainsKey('find') then
    lFind := Req.Query.Field('find').Asstring;

  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);

    if lFind <> '' then
    begin
      lCriterion := TLikeExpression.Create('ORDERNUMBER', lFind +  '%', TWhereOperator.woLike);
      lOrderItens := lSession.FindWhere<TOrderItem>(lCriterion);
    end else begin
      if (lPageIndex > 0) and (lSize > 0) then
        lOrderItens := lSession.Page<TOrderItem>(lPageIndex, lSize).Items
      else
        lOrderItens := lSession.FindAll<TOrderItem>;
    end;

    lJson := TJSONArray.Create;
    try
      for lOrderItem in lOrderItens.ToArray do
        lJson.AddElement(TJson.ObjectToJsonObject(lOrderItem));
      Res.Send(lJson.ToString);
    finally
      lJson.Free;
    end;
  finally
    lConnection.Free;
  end;
end;

procedure GetOrderItem(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lOrderItens: IList<TOrderItem>;
  lOrderItem: TOrderItem;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lId := req.Params.Field('id').AsInteger;
    lOrderItem := lSession.GetOrderItem(lid);
    Res.Send(TJson.ObjectToJsonString(lOrderItem));
  finally
    lConnection.Free;
  end;
end;

procedure SetOrderItem(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lOrderItens: IList<TOrderItem>;
  lOrderItem: TOrderItem;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lOrderItem := TJson.JsonToObject<TOrderItem>(req.Body);
    try
      lId := lSession.SetOrderItem(lOrderItem);
      Res.Send(TJson.ObjectToJsonString(lOrderItem));
    finally
      lOrderItem.Free;
    end;
  finally
    lConnection.Free;
  end;
end;

procedure DelOrderItem(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lOrderItens: IList<TOrderItem>;
  lProdct: TOrderItem;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lId := req.Params.Field('id').AsInteger;
    lSession.DelOrderItem(lId);
  finally
    lConnection.Free;
  end;
end;

procedure FindOrderItem(Req: THorseRequest; Res: THorseResponse; Next: TProc);
begin
  GetOrderItens(Req, Res, Next);
end;

end.
