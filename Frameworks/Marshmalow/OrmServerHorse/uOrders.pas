unit uOrders;

interface

uses
  Horse, Spring.Persistence.Criteria.Interfaces, uConnection, uSession,
  Spring.Persistence.Criteria.Criterion.LikeExpression, ProductModel,
  Spring.Collections, Spring.Persistence.SQL.Types, OrderModel;

procedure GetOrders(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure GetOrder(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure SetOrder(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure DelOrder(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure FindOrder(Req: THorseRequest; Res: THorseResponse; Next: TProc);


implementation

uses
  REST.Json, System.JSON;

procedure GetOrders(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lOrders: IList<TOrder>;
  lOrder: TOrder;
  lJson: TJSONArray;
  lConnection: TSystemConnection;
  lPageIndex: Integer;
  lSize: Integer;
  lFind: string;
  lCriterion: ICriterion;
begin
  Res.ContentType('application/json');

  lPageIndex := 0;
  lSize := 0;
  lFind := '';

  Req.Query.TryGetValue('pFiltro',lFind);
  if Req.Query.ContainsKey('page_index') then
    lPageIndex := Req.Query.Field('page_index').AsInteger;
  if Req.Query.ContainsKey('size') then
    lSize := Req.Query.Field('size').AsInteger;
  if Req.Query.ContainsKey('find') then
    lFind := Req.Query.Field('find').Asstring;

  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);

    if lFind <> '' then
    begin
      lCriterion := TLikeExpression.Create('ORDERNUMBER', lFind +  '%', TWhereOperator.woLike);
      lOrders := lSession.FindWhere<TOrder>(lCriterion);
    end else begin
      if (lPageIndex > 0) and (lSize > 0) then
        lOrders := lSession.Page<TOrder>(lPageIndex, lSize).Items
      else
        lOrders := lSession.FindAll<TOrder>;
    end;

    lJson := TJSONArray.Create;
    try
      for lOrder in lOrders.ToArray do
        lJson.AddElement(TJson.ObjectToJsonObject(lOrder));
      Res.Send(lJson.ToString);
    finally
      lJson.Free;
    end;
  finally
    lConnection.Free;
  end;
end;

procedure GetOrder(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lOrders: IList<TOrder>;
  lOrder: TOrder;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lId := req.Params.Field('id').AsInteger;
    lOrder := lSession.GetOrder(lid);
    Res.Send(TJson.ObjectToJsonString(lOrder));
  finally
    lConnection.Free;
  end;
end;

procedure SetOrder(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lOrders: IList<TOrder>;
  lOrder: TOrder;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lOrder := TJson.JsonToObject<TOrder>(req.Body);
    try
      lId := lSession.SetOrder(lOrder);
      Res.Send(TJson.ObjectToJsonString(lOrder));
    finally
      lOrder.Free;
    end;
  finally
    lConnection.Free;
  end;
end;

procedure DelOrder(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lOrders: IList<TOrder>;
  lProdct: TOrder;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lId := req.Params.Field('id').AsInteger;
    lSession.DelOrder(lId);
  finally
    lConnection.Free;
  end;
end;

procedure FindOrder(Req: THorseRequest; Res: THorseResponse; Next: TProc);
begin
  GetOrders(Req, Res, Next);
end;



end.
