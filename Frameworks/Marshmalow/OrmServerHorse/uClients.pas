unit uClients;

interface

uses
  Horse;

procedure GetClients(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure GetClient(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure SetClient(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure DelClient(Req: THorseRequest; Res: THorseResponse; Next: TProc);
procedure FindClient(Req: THorseRequest; Res: THorseResponse; Next: TProc);


implementation

uses
  uSession, Spring.Collections, ClientModel, System.JSON, uConnection,
  REST.Json, Spring.Persistence.Criteria.Interfaces, Spring.Persistence.Criteria.Criterion.LikeExpression,
  Spring.Persistence.SQL.Types;


procedure GetClients(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lClients: IList<TClient>;
  lClient: TClient;
  lJson: TJSONArray;
  lConnection: TSystemConnection;
  lPageIndex: Integer;
  lSize: Integer;
  lFind: string;
  lCriterion: ICriterion;
begin
  Res.ContentType('application/json');

  lPageIndex := 0;
  lSize := 0;
  lFind := '';

  Req.Query.TryGetValue('pFiltro',lFind);

  if Req.Query.ContainsKey('page_index') then
    lPageIndex := Req.Query.Field('page_index').AsInteger;
  if Req.Query.ContainsKey('size') then
    lSize := Req.Query.Field('size').AsInteger;
  if Req.Query.ContainsKey('find') then
    lFind := Req.Query.Field('find').Asstring;

  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    if lFind <> '' then
    begin
      lCriterion := TLikeExpression.Create('CLINAME', lFind +  '%', TWhereOperator.woLike);
      lClients := lSession.FindWhere<TClient>(lCriterion);
    end else begin
      if (lPageIndex > 0) and (lSize > 0) then
        lClients := lSession.Page<TClient>(lPageIndex, lSize).Items
      else
        lClients := lSession.FindAll<TClient>;
    end;

    lJson := TJSONArray.Create;
    try
      for lClient in lClients.ToArray do
        lJson.AddElement(TJson.ObjectToJsonObject(lClient));
      Res.Send(lJson.ToString);
    finally
      lJson.Free;
    end;
  finally
    lConnection.Free;
  end;
end;



procedure GetClient(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lClients: IList<TClient>;
  lClient: TClient;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lId := req.Params.Field('id').AsInteger;
    lClient := lSession.GetClient(lid);
    Res.Send(TJson.ObjectToJsonString(lClient));
  finally
    lConnection.Free;
  end;
end;


procedure SetClient(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lClients: IList<TClient>;
  lClient: TClient;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lClient := TJson.JsonToObject<TClient>(req.Body);
    try
      lId := lSession.SetClient(lClient);
      Res.Send(TJson.ObjectToJsonString(lClient));
    finally
      lClient.Free;
    end;
  finally
    lConnection.Free;
  end;
end;


procedure DelClient(Req: THorseRequest; Res: THorseResponse; Next: TProc);
var
  lSession: TSystemSession;
  lClients: IList<TClient>;
  lClient: TClient;
  lJson: TJSONArray;
  lId: Integer;
  lConnection: TSystemConnection;
begin
  Res.ContentType('application/json');
  lConnection := TSystemConnection.GetConnection;
  try
    lSession := TSystemSession.Create(lConnection.Connection);
    lId := req.Params.Field('id').AsInteger;
    lSession.DelClient(lId);
  finally
    lConnection.Free;
  end;
end;

procedure FindClient(Req: THorseRequest; Res: THorseResponse; Next: TProc);
begin
  GetClients(Req, Res, Next);
end;

end.
