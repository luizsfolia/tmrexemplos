unit uConnection;

interface

uses
  SQLiteTable3, Spring.Persistence.Adapters.SQLite, uSession,
  Spring.Persistence.Core.Interfaces, System.Classes, Vcl.Forms;

type


  TSystemConnection = class(TComponent)
  private
    FDatabase: TSQLiteDatabase;
    FConnection : IDBConnection;
//    FSession : TSystemSession;
  public

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    function Connection : IDBConnection;

    class function GetConnection : TSystemConnection;
  end;


implementation

{ TSystemConnection }

function TSystemConnection.Connection: IDBConnection;
begin
  Result := FConnection;
end;

constructor TSystemConnection.Create(AOwner: TComponent);
begin
  inherited;
//  if FDatabase = nil then
//  begin
    FDatabase := TSQLiteDatabase.Create(Self);
    FDatabase.Filename := 'tmrexample.db3';
//  end;

  FConnection := TSQLiteConnectionAdapter.Create(FDatabase);
  FConnection.AutoFreeConnection := True;
  FConnection.Connect;
//  FSession := TSystemSession.Create(FConnection);
end;

destructor TSystemConnection.Destroy;
begin
//  FSession.Free;
  FConnection.Disconnect;
//  FConnection.Free;
//  FDatabase.Free;
  inherited;
end;

class function TSystemConnection.GetConnection: TSystemConnection;
begin
  Result := TSystemConnection.Create(Application);
end;

end.
