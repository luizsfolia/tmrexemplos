object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 256
  ClientWidth = 497
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  TextHeight = 13
  object btnStart: TBitBtn
    Left = 32
    Top = 72
    Width = 145
    Height = 25
    Caption = 'Start Server'
    TabOrder = 0
    OnClick = btnStartClick
  end
  object BitBtn2: TBitBtn
    Left = 32
    Top = 33
    Width = 145
    Height = 25
    Caption = 'Construir Banco de Dados'
    TabOrder = 1
    OnClick = BitBtn2Click
  end
  object btnStop: TBitBtn
    Left = 32
    Top = 103
    Width = 145
    Height = 25
    Caption = 'Stop Server'
    Enabled = False
    TabOrder = 2
    OnClick = btnStopClick
  end
end
