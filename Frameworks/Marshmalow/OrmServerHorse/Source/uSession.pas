unit uSession;

interface

uses
  Spring.Persistence.Core.Session, Spring.Collections, OrderModel,
  OrderItemModel, ClientModel, ProductModel;


type

  TSystemSession = class(TSession)
  public
    function GetMyOrder: IList<TOrder>;
    function GetMyOrderJoin: IList<TOrder>;

    function GetClient(const pId : Integer) : TClient;
    function SetClient(const pClient : TClient) : Integer;
    function DelClient(const pId : Integer) : Boolean;

    function GetProduct(const pId : Integer) : TProduct;
    function SetProduct(const pProduct : TProduct) : Integer;
    function DelProduct(const pId : Integer) : Boolean;

    function GetOrder(const pId : Integer) : TOrder;
    function SetOrder(const pOrder : TOrder) : Integer;
    function DelOrder(const pId : Integer) : Boolean;

    function GetOrderItem(const pId : Integer) : TOrderItem;
    function SetOrderItem(const pOrder : TOrderItem) : Integer;
    function DelOrderItem(const pId : Integer) : Boolean;
  end;

implementation

uses
  Spring.Persistence.Criteria.Interfaces,
  Spring.Persistence.Criteria.Criterion.SimpleExpression,
  Spring.Persistence.SQL.Types;

{ TSystemSession }

function TSystemSession.DelClient(const pId: Integer): Boolean;
var
  lCliente: TClient;
begin
  lCliente := GetClient(pId);
  if lCliente <> nil then
  begin
    Self.Delete(lCliente);
    Result := True;
  end;
end;

function TSystemSession.DelOrder(const pId: Integer): Boolean;
var
  lOrder: TOrder;
begin
  lOrder := GetOrder(pId);
  if lOrder <> nil then
  begin
    Self.Delete(lOrder);
    Result := True;
  end;
end;

function TSystemSession.DelOrderItem(const pId: Integer): Boolean;
var
  lOrderItem: TOrderItem;
begin
  lOrderItem := GetOrderItem(pId);
  if lOrderItem <> nil then
  begin
    Self.Delete(lOrderItem);
    Result := True;
  end;
end;

function TSystemSession.DelProduct(const pId: Integer): Boolean;
var
  lProduct: TProduct;
begin
  lProduct := GetProduct(pId);
  if lProduct <> nil then
  begin
    Self.Delete(lProduct);
    Result := True;
  end;
end;

function TSystemSession.GetClient(const pId : Integer): TClient;
begin
  Result := Self.FindOne<TClient>(pId);
end;

function TSystemSession.GetMyOrder: IList<TOrder>;
var
  lCriterion: ICriterion;
  lItem: TOrder;
  lOrderItem: TOrderITem;
begin
  Result := Self.FindAll<TOrder>;
  for lItem in Result do
  begin
    lItem.Client := Self.FindOne<TClient>(lItem.ClientId);

    lCriterion := TSimpleExpression.Create('OrderId', lItem.Id, TWhereOperator.woEqual);
//    lItem.Itens := Self.FindWhere<TOrderItem>(lCriterion);
//
//    for lOrderItem in lItem.Itens do
//      lOrderItem.Product := Self.FindOne<TProduct>(lOrderItem.ProductId);
  end;
end;

function TSystemSession.GetMyOrderJoin: IList<TOrder>;
begin
end;

function TSystemSession.GetOrder(const pId: Integer): TOrder;
begin
  Result := Self.FindOne<TOrder>(pId);
end;

function TSystemSession.GetOrderItem(const pId: Integer): TOrderItem;
begin
  Result := Self.FindOne<TOrderItem>(pId);
end;

function TSystemSession.GetProduct(const pId: Integer): TProduct;
begin
  Result := Self.FindOne<TProduct>(pId);
end;

function TSystemSession.SetClient(const pClient: TClient): Integer;
begin
  if pClient.ID = 0 then
    Self.Insert(pClient)
  else
    Self.Update(pClient);
  Result := pClient.ID;
end;

function TSystemSession.SetOrder(const pOrder: TOrder): Integer;
begin
  if pOrder.ID = 0 then
    Self.Insert(pOrder)
  else
    Self.Update(pOrder);
  Result := pOrder.ID;
end;

function TSystemSession.SetOrderItem(const pOrder: TOrderItem): Integer;
begin
  if pOrder.ID = 0 then
    Self.Insert(pOrder)
  else
    Self.Update(pOrder);
  Result := pOrder.ID;
end;

function TSystemSession.SetProduct(const pProduct: TProduct): Integer;
begin
  if pProduct.ID = 0 then
    Self.Insert(pProduct)
  else
    Self.Update(pProduct);
  Result := pProduct.ID;
end;

end.
