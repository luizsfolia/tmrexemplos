unit OrderItemRepository;

interface

uses
  Spring.Collections,
  Spring.Persistence.Core.Repository.Simple, OrderItemModel;

type

  TOrderITemRepository = class(TSimpleRepository<TOrderItem,Integer>)

  end;


implementation

end.
