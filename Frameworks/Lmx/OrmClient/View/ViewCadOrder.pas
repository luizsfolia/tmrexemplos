unit ViewCadOrder;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uViewCadBase, System.Actions, FMX.ActnList, FMX.Controls.Presentation,
  FMX.Objects, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, FMX.Edit, FMX.EditBox, FMX.SpinBox,
  uLmxOrderModel, uLmxClientModel, uLmxProductModel, uLmxProxyOrderItem,
  uLmxProxyOrder;

type
  TfrmCadOrder = class(TfrmCadBase)
    Rectangle1: TRectangle;
    edtProduto: TEdit;
    Button2: TButton;
    Button4: TButton;
    lstItens: TListView;
    Rectangle3: TRectangle;
    edtCliente: TEdit;
    actAddItem: TAction;
    actDeleteItem: TAction;
    Label1: TLabel;
    Label2: TLabel;
    speQuantidade: TSpinBox;
    btnAddProd: TButton;
    btnAddCli: TButton;
    actAddProd: TAction;
    actAddCli: TAction;
    speNum: TSpinBox;
    Label3: TLabel;
    Label4: TLabel;
    speCodClient: TSpinBox;
    speProduct: TSpinBox;
    procedure actAddCliExecute(Sender: TObject);
    procedure actAddProdExecute(Sender: TObject);
    procedure actAddItemExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FProduct : TProduct;
    procedure DoAddITem;
  protected
    procedure DoGetObjectProperties; override;
    procedure DoSetObjectProperties; override;
  public
    { Public declarations }
  end;

var
  frmCadOrder: TfrmCadOrder;

implementation

uses
  uViewFindDefault, uLmxProxy.Base, uLmxProxyClient,
  uLmxProxyProduct, uLmxOrderItemModel;

{$R *.fmx}

{ TfrmCadBase1 }

procedure TfrmCadOrder.actAddCliExecute(Sender: TObject);
var
  lClient: TClient;
begin
  inherited;
  lClient :=
    TfrmFindDefault.Select<TClient>(
    function : TLmxProxyBaseDefault<TClient>
    begin
      Result := THttpClient.Proxy;
    end,
    procedure(Sender: TListView)
    begin
    end,
    procedure(Sender: TObject; Item: TListViewItem; pItemObject : TClient)
    begin
      Item.Text := pItemObject.Name;
    end,
    Self);

  if lClient <> nil then
  begin
    GetObjectAs<TOrders>.Client.DeOutro(lClient);
    GetObjectAs<TOrders>.Client_id := lClient.Id;
    lClient.Free;

    DoGetObjectProperties;
  end;
end;

procedure TfrmCadOrder.actAddItemExecute(Sender: TObject);
begin
  inherited;
  DoAddITem;
end;

procedure TfrmCadOrder.actAddProdExecute(Sender: TObject);
begin
  inherited;
  FProduct.Free;
  FProduct := TfrmFindDefault.Select<TProduct>(
    function : TLmxProxyBaseDefault<TProduct>
    begin
      Result := THttpProduct.Proxy;
    end,
    procedure(Sender: TListView)
    begin
    end,
    procedure(Sender: TObject; Item: TListViewItem; pItemObject : TProduct)
    begin
      Item.Text := pItemObject.Name;
    end,
    Self);

  DoGetObjectProperties;
end;

procedure TfrmCadOrder.DoAddITem;
var
  lItem: TOrderItem;
  lId: Integer;
begin
  lItem := TOrderItem.Create;
  lItem.Order_Id := GetObjectAs<TOrders>.Id;
  lItem.Product.DeOutro(FProduct);
  lItem.Product_Id := FProduct.Id;
  lItem.Quantity := speQuantidade.Text.ToInteger;

  THttpOrderItens.Proxy.Incluir(lItem, lId);

  GetObjectAs<TOrders>.Itens.Add(lItem);

  DoGetObjectProperties;
end;

procedure TfrmCadOrder.DoGetObjectProperties;
var
  lItem : TOrderItem;
  I: Integer;
  lItemViewList: TListViewItem;
begin
  inherited;
  speCodClient.Value := GetObjectAs<TOrders>.Client_id;
  edtCliente.Text := GetObjectAs<TOrders>.Client.Name;
  speNum.Value := GetObjectAs<TOrders>.Number;

  if FProduct <> nil then
  begin
    edtProduto.Text := FProduct.Name;
    speProduct.Value := FProduct.Id;
  end;

  lstItens.BeginUpdate;
  try
    lstItens.Items.Clear;
    for lItem in GetObjectAs<TOrders>.Itens do
    begin
      THttpProduct.Proxy.Carregar(lItem.Product.Id, lItem.Product);

      lItemViewList := lstItens.Items.Add;
      lItemViewList.Text := lItem.Product.Name;
      lItemViewList.Detail := lItem.Quantity.ToString;
    end;
  finally
    lstItens.EndUpdate;
  end;
end;

procedure TfrmCadOrder.DoSetObjectProperties;
begin
  inherited;
  GetObjectAs<TOrders>.Client_id := speCodClient.Text.ToInteger;
  GetObjectAs<TOrders>.Number := speNum.Text.ToInteger;;
end;

procedure TfrmCadOrder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FProduct.Free;
end;

end.
