unit uViewFindDefault;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, System.Actions,
  FMX.ActnList, FMX.Edit, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, uViewCadBase, uLmxProxy.Base, uLmxAttributes,
  uLmxInterfaces, uLmxCore, FMX.Dialogs, uLmxProxy, FMX.Layouts, FMX.ListBox;

type

  TOnGetData<T : class, constructor> = reference to procedure(Sender: TObject; Item: TListViewItem; pItemObject : T);
  TOnGetConfigList = reference to procedure(Sender: TListView);
  TOnGetProxy<T : TBaseTabelaPadrao, constructor> = reference to function : TLmxProxyBaseDefault<T>;

  TStateForm = (sfFind, sfPendent);

  TfrmFindDefault = class(TForm)
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    btnFind: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    edtFind: TEdit;
    ActionList1: TActionList;
    actAdd: TAction;
    actDelete: TAction;
    actFind: TAction;
    actSave: TAction;
    actSelect: TAction;
    lstData: TListView;
    stbStatus: TStatusBar;
    lblQuantidade: TLabel;
    Button1: TButton;
    actEdit: TAction;
    procedure actAddExecute(Sender: TObject);
    procedure actFindExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure actSelectExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure actEditExecute(Sender: TObject);
  private type
    TInternalOnGetData = reference to procedure(Sender: TObject; Item: TListViewItem);
    TInternalOnGetItem = reference to function(Sender: TObject; Item: TListItem) : TObject;
    TInternalOnNewItem = reference to function : TObject;
    TInternalOnAddItem = TProc<TObject>;
    TInternalOnSaveItens = TProc;
    TInternalOnRemoveItem = TProc<TObject>;
    TInternalOnSelectItem = TProc<TObject>;
    TInternalOnSaveItem = TProc<TObject>;
  private
    FState : TStateForm;
    FCadClass: TfrmCadBaseClass;

    FInternalOnGetData : TInternalOnGetData;
    FInternalOnReload : TProc;
    FInternalOnFind : TProc<string>;
    FInternalOnEditItem : TInternalOnGetItem;
    FInternalOnNewItem : TInternalOnNewItem;
    FInternalOnAddItem : TInternalOnAddItem;
    FInternalOnSaveItens : TInternalOnSaveItens;
    FInternalOnRemoveItem : TInternalOnRemoveItem;
    FInternalOnGetConfigList : TOnGetConfigList;
    FInternalOnSelectItem : TProc<TObject>;
    FInternalOnSaveItem : TInternalOnSaveItem;

  protected
    procedure DoRepaint<T : class, constructor>(const pList : ILmxEnumerable);
    procedure SetOnInternalFind(const pOnGetData : TProc<string>);
    procedure SetOnInternalGetData(const pOnGetData : TInternalOnGetData);
    procedure SetOnInternalGetConfigList(const pOnGetConfigList : TOnGetConfigList);
    procedure SetOnInternalEditItem(const pOnGetData : TInternalOnGetItem);
    procedure SetOnInternalNewItem(const pOnGetData : TInternalOnNewItem);
    procedure SetOnInternalAddItem(const pOnGetData : TInternalOnAddItem);
    procedure SetOnInternalSaveItens(const pOnGetData : TInternalOnSaveItens);
    procedure SetOnInternalSaveItem(const pOnGetData : TInternalOnSaveItem);
    procedure SetOnInternalRemoveItem(const pOnGetData : TInternalOnRemoveItem);
    procedure SetOnSelectItem(const pOnGetData : TInternalOnSelectItem);

    procedure SetOnReload(const pOnReload : TProc);

    procedure DoAddNewItem(const pItem : TObject);
    procedure DoReload;
    procedure DoAdd;
    procedure DoEdit;
    procedure DoSave;
    procedure DoSaveItem(const pItem : TObject);
    procedure DoDelete;
    procedure DoSelectItem;
  public

    property CadClass: TfrmCadBaseClass read FCadClass write FCadClass;

    class function Select<T : TBaseTabelaPadrao, constructor>(const pOnGetProxy : TOnGetProxy<T>; const pOnGetConfigList : TOnGetConfigList;
      const pOnGetData : TOnGetData<T>; Aowner : TComponent = nil) : T;
    class function Edit<T : TBaseTabelaPadrao, constructor>(const pOnGetProxy : TOnGetProxy<T>; const pOnGetConfigList : TOnGetConfigList;
      const pOnGetData : TOnGetData<T>; const pCadClass : TfrmCadBaseClass = nil; Aowner : TComponent = nil) : T;
  end;

var
  frmFindDefault: TfrmFindDefault;

implementation

uses
  System.Rtti;


{$R *.fmx}

procedure TfrmFindDefault.actAddExecute(Sender: TObject);
begin
  DoAdd;
end;

procedure TfrmFindDefault.actDeleteExecute(Sender: TObject);
begin
  DoDelete;
end;

procedure TfrmFindDefault.actEditExecute(Sender: TObject);
var
  lValue: TValue;
  lITem: TObject;
begin
  DoEdit;
//  if FCadClass <> nil then
//  begin
//    lITem := nil;
//    if Assigned(FInternalOnEditItem) then
//      lITem := FInternalOnEditItem(lstData, AItem);
//    if lITem <> nil then
//    begin
//      if FCadClass.Edit(lITem) then
//      begin
//        FState := sfPendent;
//        DoSaveItem(lITem);
//      end;
//    end;
//  end;

//  lValue := AItem.Data[0];
//  if not lValue.IsEmpty then
//  begin
//
//  end;
end;

procedure TfrmFindDefault.actFindExecute(Sender: TObject);
begin
  DoReload;
end;

procedure TfrmFindDefault.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  actSelect.Enabled := FCadClass = nil;
  actAdd.Enabled := FCadClass <> nil;
  actDelete.Enabled := FCadClass <> nil;
  actEdit.Enabled := FCadClass <> nil;
end;

procedure TfrmFindDefault.actSaveExecute(Sender: TObject);
begin
  DoSave;
end;

procedure TfrmFindDefault.actSelectExecute(Sender: TObject);
begin
  DoSelectItem;
end;

procedure TfrmFindDefault.DoAdd;
var
  lItem: TObject;
  lOk: Boolean;
begin
  lItem := FInternalOnNewItem;
  try
    lOk := FCadClass.Edit(lItem);
    if lOk then
      DoAddNewItem(lItem);
  finally
    if not lOk then
      lItem.Free;
  end;
end;

procedure TfrmFindDefault.DoAddNewItem(const pItem: TObject);
begin
  if Assigned(FInternalOnAddItem) then
  begin
    FInternalOnAddItem(pITem);
    FState := sfPendent;
  end;
end;

procedure TfrmFindDefault.DoDelete;
var
  lITem: TObject;
begin
  lITem := nil;
  if Assigned(FInternalOnEditItem) then
    lITem := FInternalOnEditItem(lstData, lstData.Selected);
  if lITem <> nil then
  begin
    if Assigned(FInternalOnRemoveItem) then
      FInternalOnRemoveItem(lITem);
  end;
end;

procedure TfrmFindDefault.DoEdit;
var
  lITem: TObject;
begin
  lITem := nil;
  if Assigned(FInternalOnEditItem) then
    lITem := FInternalOnEditItem(lstData, lstData.Selected);
  if lITem <> nil then
  begin
    if FCadClass.Edit(lITem) then
    begin
      FState := sfPendent;
      DoSaveItem(lItem);
    end;
  end;
end;

procedure TfrmFindDefault.DoReload;
begin
  if edtFind.Text <> '' then
  begin
    if Assigned(FInternalOnFind) then
      FInternalOnFind(edtFind.Text);
  end else begin
    if Assigned(FInternalOnReload) then
      FInternalOnReload;
  end;
end;

procedure TfrmFindDefault.DoRepaint<T>(const pList: ILmxEnumerable);
var
  lItemList: TListBoxItem;
  lItem : TObject;
  I: Integer;
  lItemViewList: TListViewItem;
begin
  lstData.BeginUpdate;
  try
    lstData.Items.Clear;
    for I := 0 to pList.Count - 1 do
    begin
      lItem := pList.GetItemObject(I);

      lItemViewList := lstData.Items.Add;
//      lItemViewList.Text :=
//      lItemViewList.Detail :=

      if Assigned(FInternalOnGetData) then
        FInternalOnGetData(lstData, lItemViewList);

//      lItemViewList.Data[0] := lItem;

//      lstData.AddObject(lItemList);
    end;
  finally
    lstData.EndUpdate;
  end;

//  lsbData.BeginUpdate;
//  try
//    for I := 0 to pList.Count - 1 do
//    begin
//      lItem := pList.GetItemObject(I);
//      lItemList := TListBoxItem.Create(lsbData);
//      lItemList.Data := lItem;
//
//      lsbData.AddObject(lItemList);
//    end;
//  finally
//    lsbData.EndUpdate;
//  end;

  lblQuantidade.Text := Format('Total: %d', [pList.Count]);
end;

procedure TfrmFindDefault.DoSave;
begin
  if Assigned(FInternalOnSaveItens) then
    FInternalOnSaveItens;
  FState := sfFind;
end;


procedure TfrmFindDefault.DoSaveItem(const pItem: TObject);
begin
  if Assigned(FInternalOnSaveItem) then
    FInternalOnSaveItem(pITem);
  FState := sfFind;
end;

procedure TfrmFindDefault.DoSelectItem;
var
  lITem: TObject;
begin
  lITem := nil;
  if Assigned(FInternalOnEditItem) then
    lITem := FInternalOnEditItem(lstData, lstData.Selected);
  if lITem <> nil then
  begin
    if Assigned(FInternalOnSelectItem) then
      FInternalOnSelectItem(lITem);
    FState := sfFind;
    Close;
  end;
end;

class function TfrmFindDefault.Edit<T>(const pOnGetProxy: TOnGetProxy<T>;
  const pOnGetConfigList: TOnGetConfigList; const pOnGetData: TOnGetData<T>;
  const pCadClass: TfrmCadBaseClass;
  Aowner: TComponent): T;
var
  lItens: TBaseList<T>;
  lForm: TfrmFindDefault;
  lITemSelected : T;
begin
  lITemSelected := nil;
  lForm := Self.Create(Aowner);
  try
    lForm.CadClass := pCadClass;

    lItens := TBaseList<T>.Create;

    lForm.SetOnInternalGetConfigList(pOnGetConfigList);
    lForm.SetOnInternalGetData(
      procedure (Sender: TObject; Item: TListViewItem)
      var
        lItem: T;
      begin
        lItem := T(lItens.GetItemObject(Item.Index));
        if Assigned(pOnGetData) then
          pOnGetData(Sender, Item, lItem);
      end);

    lForm.SetOnInternalFind(
      procedure(pArg : string)
      var
        lRetorno: string;
      begin
        FreeAndNil(litens);
//        lItens := pOnGetProxy.Find(pArg, 'json');
//        lForm.DoRepaint<T>(lItens);
      end);


    lForm.SetOnReload(
      procedure
      begin
        FreeAndNil(litens);
        litens := pOnGetProxy.Listar;
        lForm.DoRepaint<T>(lItens);
      end);

    lForm.SetOnInternalEditItem(
      function (Sender: TObject; Item: TListItem) : TObject
      begin
        Result := T(lItens.GetItemObject(Item.Index));
        pOnGetProxy.Carregar(T(Result).Id, T(Result));
      end);

    lForm.SetOnInternalRemoveItem(
      procedure (Item : TObject)
      begin
//        pOnGetProxy.Excluir(T(Item));
//        lItens.Remover(T(Item));
//        lForm.DoRepaint<T>(lItens);
      end);

    lForm.SetOnInternalNewItem(
      function : TObject
      begin
        Result := T.Create;
      end);

    lForm.SetOnInternalAddItem(
      procedure (Item : TObject)
      var
        lId: Integer;
      begin
        lItens.Add(T(Item));
        pOnGetProxy.Incluir(T(Item), lId);
        T(Item).Id := lId;
        lForm.DoRepaint<T>(lItens);
      end);

    lForm.SetOnInternalSaveItens(
      procedure
      var
        lItem: T;
      begin
        for lItem in lITens do
          pOnGetProxy.Salvar(lItem);
        lForm.DoRepaint<T>(lItens);
      end);


    lForm.SetOnInternalSaveItem(
      procedure (item : TObject)
      begin
        if pOnGetProxy.Salvar(T(item)) then
          lForm.DoRepaint<T>(lItens);
      end);

    lForm.SetOnSelectItem(
      procedure (Item : TObject)
      var
        lItem: T;
      begin
        lITemSelected := T.Create;
        TBaseTabelaPadrao(lITemSelected).DeOutro(TBase(Item));
      end);

    lForm.ShowModal;
    Result := lITemSelected;
  finally
    lItens.Free;
    lForm.Free;
  end;
end;

class function TfrmFindDefault.Select<T>(const pOnGetProxy : TOnGetProxy<T>;
  const pOnGetConfigList: TOnGetConfigList; const pOnGetData: TOnGetData<T>;
  Aowner: TComponent): T;
begin
  Result :=
    TfrmFindDefault.Edit<T>(
      pOnGetProxy,
      pOnGetConfigList,
      pOnGetData,
      nil,
      Aowner)
end;

procedure TfrmFindDefault.SetOnInternalAddItem(
  const pOnGetData: TInternalOnAddItem);
begin
  FInternalOnAddItem := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalEditItem(
  const pOnGetData: TInternalOnGetItem);
begin
  FInternalOnEditItem := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalFind(const pOnGetData: TProc<string>);
begin
  FInternalOnFind := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalGetConfigList(
  const pOnGetConfigList: TOnGetConfigList);
begin
  FInternalOnGetConfigList := pOnGetConfigList;
end;

procedure TfrmFindDefault.SetOnInternalGetData(
  const pOnGetData: TInternalOnGetData);
begin
  FInternalOnGetData := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalNewItem(
  const pOnGetData: TInternalOnNewItem);
begin
  FInternalOnNewItem := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalRemoveItem(
  const pOnGetData: TInternalOnRemoveItem);
begin
  FInternalOnRemoveItem := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalSaveItem(
  const pOnGetData: TInternalOnSaveItem);
begin
  FInternalOnSaveItem := pOnGetData;
end;

procedure TfrmFindDefault.SetOnInternalSaveItens(
  const pOnGetData: TInternalOnSaveItens);
begin
  FInternalOnSaveItens := pOnGetData;
end;

procedure TfrmFindDefault.SetOnReload(const pOnReload: TProc);
begin
  FInternalOnReload := pOnReload;
end;

procedure TfrmFindDefault.SetOnSelectItem(
  const pOnGetData: TInternalOnSelectItem);
begin
  FInternalOnSelectItem := pOnGetData;
end;

end.
