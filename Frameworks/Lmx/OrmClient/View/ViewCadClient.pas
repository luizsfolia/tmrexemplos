unit ViewCadClient;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uViewCadBase, System.Actions, FMX.ActnList, FMX.Controls.Presentation,
  FMX.Objects, FMX.Edit, uLmxClientModel;

type
  TfrmCadClient = class(TfrmCadBase)
    edtName: TEdit;
    Label1: TLabel;
  private
  protected
    procedure DoGetObjectProperties; override;
    procedure DoSetObjectProperties; override;
  public
    { Public declarations }
  end;

var
  frmCadClient: TfrmCadClient;

implementation

{$R *.fmx}

{ TfrmCadCliente }

procedure TfrmCadClient.DoGetObjectProperties;
begin
  inherited;
  edtName.Text := GetObjectAs<TClient>.Name;
end;

procedure TfrmCadClient.DoSetObjectProperties;
begin
  inherited;
  GetObjectAs<TClient>.Name := edtName.Text;
end;

end.
