unit uViewCadBase;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Actions,
  FMX.ActnList, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects;

type
  TfrmCadBase = class(TForm)
    Rectangle2: TRectangle;
    Button5: TButton;
    ActionList1: TActionList;
    actOk: TAction;
    actCancel: TAction;
    Button1: TButton;
    procedure actOkExecute(Sender: TObject);
    procedure actCancelExecute(Sender: TObject);
  private
    FObject : TObject;
  protected
    procedure SetObject(pObject : TObject);
    function GetObjectAs<T : Class, constructor> : T;

    procedure DoGetObjectProperties; virtual;
    procedure DoSetObjectProperties; virtual;
  public
    class function Edit<T : Class, constructor>(const pObject: T): Boolean;
  end;

  TfrmCadBaseClass = class of TfrmCadBase;

var
  frmCadBase: TfrmCadBase;

implementation

{$R *.fmx}

{ TfrmCadBase }

procedure TfrmCadBase.actCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmCadBase.actOkExecute(Sender: TObject);
begin
  DoSetObjectProperties;
  ModalResult := mrOk;
end;

procedure TfrmCadBase.DoGetObjectProperties;
begin

end;

procedure TfrmCadBase.DoSetObjectProperties;
begin

end;

class function TfrmCadBase.Edit<T>(const pObject: T): Boolean;
var
  lForm: TfrmCadBase;
begin
  LForm := Self.Create(Application);
  try
    lForm.SetObject(pObject);
    Result := (lForm.ShowModal = mrOk);
  finally
    lForm.Free;
  end;
end;

function TfrmCadBase.GetObjectAs<T>: T;
begin
  Result := T(FObject);
end;

procedure TfrmCadBase.SetObject(pObject: TObject);
begin
  FObject := pObject;
  DoGetObjectProperties;
end;

end.
