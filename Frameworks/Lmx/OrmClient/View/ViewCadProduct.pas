unit ViewCadProduct;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  uViewCadBase, System.Actions, FMX.ActnList, FMX.Controls.Presentation,
  FMX.Objects, FMX.EditBox, FMX.SpinBox, FMX.Edit, uLmxProductModel;

type
  TfrmCadProduct = class(TfrmCadBase)
    edtName: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    sprPrice: TSpinBox;
    speQuantity: TSpinBox;
  private
  protected
    procedure DoGetObjectProperties; override;
    procedure DoSetObjectProperties; override;
  public
    { Public declarations }
  end;

var
  frmCadProduct: TfrmCadProduct;

implementation

{$R *.fmx}

{ TfrmCadProduct }

procedure TfrmCadProduct.DoGetObjectProperties;
begin
  inherited;
  edtName.Text := GetObjectAs<TProduct>.Name;
  sprPrice.Text := CurrToStr(GetObjectAs<TProduct>.Price);
  speQuantity.Value := GetObjectAs<TProduct>.Quantity;
end;

procedure TfrmCadProduct.DoSetObjectProperties;
begin
  inherited;
  GetObjectAs<TProduct>.Name := edtName.Text;
  GetObjectAs<TProduct>.Price := sprPrice.Value;
  GetObjectAs<TProduct>.Quantity := speQuantity.Text.ToInteger;
end;

end.
