unit uLmxProxyClient;

interface


uses
  System.SysUtils, uLmxProxy.Base, uLmxProxy, uLmxClientModel;

type

  [TLmxRota('Clients')]
  TLmxProxyClient = class(TLmxProxyBaseDefault<TClient>);

  THttpClient = TLmxHttpProxyBase<TLmxProxyClient>;

implementation

end.
