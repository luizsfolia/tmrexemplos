unit uLmxProxyOrder;

interface


uses
  System.SysUtils, uLmxProxy.Base, uLmxProxy, uLmxOrderModel,
  uLmxOrderItemModel;

type

  [TLmxRota('Orders')]
  TLmxProxyOrder = class(TLmxProxyBaseDefault<TOrders>)
  public
    function ListByOrder(const Id : Integer) : TOrderItensList;
  end;

  THttpOrder = TLmxHttpProxyBase<TLmxProxyOrder>;

implementation

{ TLmxProxyOrder }

function TLmxProxyOrder.ListByOrder(const Id: Integer): TOrderItensList;
begin
  Result := NewHttpRest
    .Get(Format('%d/Itens', [Id]))
//    .Get('1/Itens')
    .AsObject<TOrderItensList>;
end;

end.
