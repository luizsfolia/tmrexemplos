unit uLmxProxyProduct;

interface

uses
  System.SysUtils, uLmxProxy.Base, uLmxProxy, uLmxProductModel;

type

  [TLmxRota('Products')]
  TLmxProxyProduct = class(TLmxProxyBaseDefault<TProduct>);

  THttpProduct = TLmxHttpProxyBase<TLmxProxyProduct>;

implementation

end.
