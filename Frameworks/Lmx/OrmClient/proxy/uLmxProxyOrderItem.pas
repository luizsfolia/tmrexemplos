unit uLmxProxyOrderItem;

interface

uses
  System.SysUtils, uLmxProxy.Base, uLmxProxy, uLmxOrderItemModel;

type

  [TLmxRota('OrderItens')]
  TLmxProxyOrderItens = class(TLmxProxyBaseDefault<TOrderItem>);

  THttpOrderItens = TLmxHttpProxyBase<TLmxProxyOrderItens>;


implementation

end.
