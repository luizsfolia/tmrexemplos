program FMX_OrmClient;

uses
  System.StartUpCopy,
  FMX.Forms,
  uLmxMainOrmClient in 'uLmxMainOrmClient.pas' {frmMainOrmClient},
  uLmxOrderModel in 'model\uLmxOrderModel.pas',
  uLmxProductModel in 'model\uLmxProductModel.pas',
  uLmxClientModel in 'model\uLmxClientModel.pas',
  uLmxProxyClient in 'proxy\uLmxProxyClient.pas',
  uLmxProxyProduct in 'proxy\uLmxProxyProduct.pas',
  uViewFindDefault in 'View\uViewFindDefault.pas' {frmFindDefault},
  uViewCadBase in 'View\uViewCadBase.pas' {frmCadBase},
  ViewCadClient in 'View\ViewCadClient.pas' {frmCadClient},
  ViewCadProduct in 'View\ViewCadProduct.pas' {frmCadProduct},
  ViewCadOrder in 'View\ViewCadOrder.pas' {frmCadOrder},
  uLmxProxyOrder in 'proxy\uLmxProxyOrder.pas',
  uLmxOrderItemModel in 'model\uLmxOrderItemModel.pas',
  uLmxProxyOrderItem in 'proxy\uLmxProxyOrderItem.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMainOrmClient, frmMainOrmClient);
  Application.Run;
end.
