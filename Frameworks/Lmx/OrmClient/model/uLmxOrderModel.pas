unit uLmxOrderModel;

interface

uses
  uLmxCore, uLmxAttributes, System.Math, Generics.Collections, System.Classes,
  uLmxClientModel, uLmxOrderItemModel;


type

  [TLmxAttributeMetadata]
  TOrders = class(TBaseTabelaPadrao)
  private
    FDate: TDateTime;
    FNumber: Integer;
    FClient: TClient;
    FItens: TOrderItensList;
    FClient_id: Integer;
    procedure SetItens(const Value: TOrderItensList);
  protected
    procedure Inicializar; override;
    procedure Finalizar; override;
  public
    [TLmxAttributeMetadata]
    property Date: TDateTime read FDate write FDate;
    [TLmxAttributeMetadata]
    property Number: Integer read FNumber write FNumber;

    [TLmxAttributeMetadataForeignKey]
    property Client: TClient read FClient;
    property Client_id: Integer read FClient_id write FClient_id;

    property Itens : TOrderItensList read FItens write SetItens;
  end;

  TOrdersList = class(TBaseList<TOrders>);

implementation

{ TOrder }

procedure TOrders.Finalizar;
begin
  inherited;
  FItens.Free;
end;

procedure TOrders.Inicializar;
begin
  inherited;
  DoAdicionarFilho(TClient, FClient);
  FItens := TOrderItensList.Create;
end;
procedure TOrders.SetItens(const Value: TOrderItensList);
begin
  FItens.Free;
  FItens := Value;
end;

end.
