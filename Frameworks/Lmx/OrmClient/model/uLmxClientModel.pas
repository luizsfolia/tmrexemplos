unit uLmxClientModel;

interface

uses
  uLmxCore, uLmxAttributes, System.Math, Generics.Collections, System.Classes;

type

  [TLmxAttributeMetadata]
  TClient = class(TBaseTabelaPadrao)
  private
    FName: string;
  public
    [TLmxAttributeMetadata(50)]
    property Name : string read FName write FName;
  end;

  TClients = class(TBaseList<TClient>);


implementation

end.
