unit uLmxOrderItemModel;

interface

uses
  uLmxCore, uLmxAttributes, System.Math, Generics.Collections, System.Classes,
  uLmxClientModel, uLmxProductModel;

type

  [TLmxAttributeMetadata]
  TOrderItem = class(TBaseTabelaPadrao)
  private
    FProduct_Id: Integer;
    FPrice: Double;
    FOrder_Id: Integer;
    FQuantity: Integer;
    FProduct: TProduct;
  protected
    procedure Inicializar; override;
  public
    [TLmxAttributeMetadata]
    property Price: Double read FPrice write FPrice;
    [TLmxAttributeMetadata]
    property Quantity: Integer read FQuantity write FQuantity;

    [TLmxAttributeMetadata]
    property Order_Id: Integer read FOrder_Id write FOrder_Id;

    [TLmxAttributeMetadataForeignKey]
    property Product: TProduct read FProduct;
    property Product_Id: Integer read FProduct_Id write FProduct_Id;
  end;

  TOrderItensList = class(TBaseList<TOrderItem>);

implementation

{ TOrderItem }

procedure TOrderItem.Inicializar;
begin
  inherited;
  DoAdicionarFilho(TProduct,FProduct);
end;

end.
