unit uLmxMainOrmClient;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Actions,
  FMX.ActnList, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Edit, uLmxProxy.Base,
  uLmxOrderModel;

type
  TfrmMainOrmClient = class(TForm)
    ActionList1: TActionList;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    actProducts: TAction;
    actClients: TAction;
    actOrder: TAction;
    edtServer: TEdit;
    procedure actClientsExecute(Sender: TObject);
    procedure actProductsExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure actOrderExecute(Sender: TObject);
  private
    procedure DoConfig;
  public
    { Public declarations }
  end;

var
  frmMainOrmClient: TfrmMainOrmClient;

implementation

uses
  uViewFindDefault, ViewCadClient, FMX.ListView, uLmxClientModel,
  uLmxProxyClient, FMX.ListView.Types, FMX.ListView.Appearances,
  uLmxProductModel, uLmxProxyProduct, ViewCadProduct, uLmxProxyOrder,
  ViewCadOrder;

{$R *.fmx}

procedure TfrmMainOrmClient.actClientsExecute(Sender: TObject);
begin
  DoConfig;
  TfrmFindDefault.Edit<TClient>(
    function : TLmxProxyBaseDefault<TClient>
    begin
      Result := THttpClient.Proxy;
    end,
    procedure(Sender: TListView)
    begin
    end,
    procedure(Sender: TObject; Item: TListViewItem; pItemObject : TClient)
    begin
      Item.Text := pItemObject.Name;
      Item.Detail := pItemObject.ID.ToString;
    end,
    TfrmCadClient,
    Self);
end;

procedure TfrmMainOrmClient.actOrderExecute(Sender: TObject);
begin
  DoConfig;
  TfrmFindDefault.Edit<TOrders>(
    function : TLmxProxyBaseDefault<TOrders>
    begin
      Result := THttpOrder.Proxy;
    end,
    procedure(Sender: TListView)
    begin
    end,
    procedure(Sender: TObject; Item: TListViewItem; pItemObject : TOrders)
    begin
      Item.Text := pItemObject.Number.ToString;
      Item.Detail := pItemObject.Client.Name;
    end,
    TfrmCadOrder,
    Self);
end;

procedure TfrmMainOrmClient.actProductsExecute(Sender: TObject);
begin
  DoConfig;
  TfrmFindDefault.Edit<TProduct>(
    function : TLmxProxyBaseDefault<TProduct>
    begin
      Result := THttpProduct.Proxy;
    end,
    procedure(Sender: TListView)
    begin
    end,
    procedure(Sender: TObject; Item: TListViewItem; pItemObject : TProduct)
    begin
      Item.Text := pItemObject.Name;
      Item.Detail := pItemObject.ID.ToString;
    end,
    TfrmCadProduct,
    Self);
end;

procedure TfrmMainOrmClient.DoConfig;
begin
  TLmxProxyHttpRestConfig.Default.ServerUrl := edtServer.Text;
end;

procedure TfrmMainOrmClient.FormCreate(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown := True;
end;

end.
