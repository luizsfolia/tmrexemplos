object frmMainLmx: TfrmMainLmx
  Left = 0
  Top = 0
  Caption = 'frmMainLmx'
  ClientHeight = 337
  ClientWidth = 649
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 24
    Top = 96
    Width = 105
    Height = 49
    Action = actProducts
    TabOrder = 0
  end
  object Button3: TButton
    Left = 24
    Top = 168
    Width = 105
    Height = 49
    Action = actCadClient
    TabOrder = 1
  end
  object Button4: TButton
    Left = 24
    Top = 232
    Width = 105
    Height = 49
    Action = actOrder
    TabOrder = 2
  end
  object Button2: TButton
    Left = 24
    Top = 24
    Width = 105
    Height = 49
    Action = RebuildDatabaseAction
    TabOrder = 3
  end
  object MainActionList: TActionList
    Left = 408
    Top = 200
    object RebuildDatabaseAction: TAction
      Category = 'Database'
      Caption = 'Rebuild Database'
      OnExecute = RebuildDatabaseActionExecute
    end
    object actProducts: TAction
      Category = 'Cads'
      Caption = 'Cad Products'
      OnExecute = actProductsExecute
    end
    object actCadClient: TAction
      Category = 'Cads'
      Caption = 'Cad Client'
      OnExecute = actCadClientExecute
    end
    object actOrder: TAction
      Category = 'Cads'
      Caption = 'Order'
      OnExecute = actOrderExecute
    end
  end
end
