unit uLmxOrderModel;

interface

uses
  uLmxCore, uLmxAttributes, System.Math, Generics.Collections, System.Classes,
  uLmxClientModel;

type

  [TLmxAttributeMetadata]
  TOrders = class(TBaseTabelaPadrao)
  private
    FNome: string;
    FClient_id: Integer;
    FDate: TDateTime;
    FNumber: Integer;
    FClient: TClient;
  protected
    procedure Inicializar; override;
  public
    [TLmxAttributeMetadata(50)]
    property Nome : string read FNome write FNome;
//    [TLmxAttributeMetadata]
    property Client_id: Integer read FClient_id write FClient_id;
    [TLmxAttributeMetadata]
    property Date: TDateTime read FDate write FDate;
    [TLmxAttributeMetadata]
    property Number: Integer read FNumber write FNumber;

    [TLmxAttributeMetadataForeignKey]
    property Client: TClient read FClient;
  end;

  TOrdersList = class(TBaseList<TOrders>);

implementation

{ TOrder }

procedure TOrders.Inicializar;
begin
  inherited;
  DoAdicionarFilho(TClient, FClient);
end;

end.
