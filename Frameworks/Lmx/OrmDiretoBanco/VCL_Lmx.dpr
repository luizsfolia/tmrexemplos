program VCL_Lmx;









uses
  Vcl.Forms,
  frmMain in 'frmMain.pas' {frmMainLmx},
  uLmxClientModel in 'Source\uLmxClientModel.pas',
  uLmxProductModel in 'Source\uLmxProductModel.pas',
  uLmxOrderModel in 'Source\uLmxOrderModel.pas',
  ViewCadProduct in 'View\ViewCadProduct.pas',
  ViewFindDefault in 'View\ViewFindDefault.pas',
  ViewOrder in 'View\ViewOrder.pas',
  ViewCadClient in 'View\ViewCadClient.pas',
  ViewCadDefault in 'View\ViewCadDefault.pas',
  uLmxBaseView in '..\..\..\..\..\lmx\lmxpackage\view\uLmxBaseView.pas' {LmxBaseView},
  uLmxBaseConsultaView in '..\..\..\..\..\lmx\lmxpackage\view\uLmxBaseConsultaView.pas' {LmxBaseConsultaView},
  uLmxConsultaGenericaView in '..\..\..\..\..\lmx\lmxpackage\view\uLmxConsultaGenericaView.pas' {LmxConsultaGenericaView};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMainLmx, frmMainLmx);
  Application.CreateForm(TLmxBaseView, LmxBaseView);
  Application.CreateForm(TLmxBaseConsultaView, LmxBaseConsultaView);
  Application.CreateForm(TLmxConsultaGenericaView, LmxConsultaGenericaView);
  Application.Run;
end.
