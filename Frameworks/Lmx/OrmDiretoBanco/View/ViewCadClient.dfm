inherited frmCadClient: TfrmCadClient
  Caption = 'Cad Client'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlData: TPanel
    object NameLabel: TLabel
      Left = 16
      Top = 35
      Width = 31
      Height = 13
      Caption = 'Name:'
    end
    object NameEdit: TEdit
      Left = 80
      Top = 32
      Width = 309
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      TextHint = 'Name'
    end
  end
end
