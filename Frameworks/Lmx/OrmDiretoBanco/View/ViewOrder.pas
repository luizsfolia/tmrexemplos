unit ViewOrder;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ViewCadDefault, System.Actions,
  Vcl.ActnList, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Samples.Spin, Vcl.Mask,
  Vcl.ComCtrls, Spring.Collections;

type
  TfrmOrder = class(TfrmCadDefault)
    QuantityLabel: TLabel;
    NumberSpinEdit: TSpinEdit;
    ClientName: TMaskEdit;
    PriceLabel: TLabel;
    speClientId: TSpinEdit;
    lstData: TListView;
    Label1: TLabel;
    speItem: TSpinEdit;
    edtITem: TMaskEdit;
    SpinEdit1: TSpinEdit;
    Label2: TLabel;
    Button1: TButton;
    actAddItem: TAction;
    procedure actAddItemExecute(Sender: TObject);
  private
//    FItens : IList<TOrderItem>;
  protected
    procedure DoGetObjectProperties; override;
    procedure DoSetObjectProperties; override;
  public
    { Public declarations }
  end;

var
  frmOrder: TfrmOrder;

implementation

{$R *.dfm}

uses uLmxClientModel, uLmxOrderModel,
  uLmxProductModel;

procedure TfrmOrder.actAddItemExecute(Sender: TObject);
begin
  inherited;
//
end;

procedure TfrmOrder.DoGetObjectProperties;
begin
  inherited;
  NumberSpinEdit.Value := GetObjectAs<TOrders>.Number;
  speClientId.Value := GetObjectAs<TOrders>.Client_id;
//  ClientName.Text := GetObjectAs<TOrder>.;
end;

procedure TfrmOrder.DoSetObjectProperties;
begin
  inherited;
  GetObjectAs<TOrders>.Number := NumberSpinEdit.Value;
  GetObjectAs<TOrders>.Client_id := speClientId.Value;
end;

end.
