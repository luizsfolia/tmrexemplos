unit ViewCadProduct;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ViewCadDefault, System.Actions,
  Vcl.ActnList, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Samples.Spin, Vcl.Mask,
  uLmxProductModel;

type
  TfrmCadProduct = class(TfrmCadDefault)
    NameLabel: TLabel;
    NameEdit: TEdit;
    PriceLabel: TLabel;
    PriceMaskEdit: TMaskEdit;
    QuantityLabel: TLabel;
    QuantitySpinEdit: TSpinEdit;
  protected
    procedure DoGetObjectProperties; override;
    procedure DoSetObjectProperties; override;
  public
    { Public declarations }
  end;

var
  frmCadProduct: TfrmCadProduct;

implementation

{$R *.dfm}



{ TfrmCadProduct }

procedure TfrmCadProduct.DoGetObjectProperties;
begin
  inherited;
  NameEdit.Text := GetObjectAs<TProduct>.Name;
  PriceMaskEdit.Text := CurrToStr(GetObjectAs<TProduct>.Price);
  QuantitySpinEdit.Value := GetObjectAs<TProduct>.Quantity;
end;

procedure TfrmCadProduct.DoSetObjectProperties;
begin
  inherited;
  GetObjectAs<TProduct>.Name := NameEdit.Text;
  GetObjectAs<TProduct>.Price := StrToCurr(PriceMaskEdit.Text);
  GetObjectAs<TProduct>.Quantity := QuantitySpinEdit.Value;
end;

end.
