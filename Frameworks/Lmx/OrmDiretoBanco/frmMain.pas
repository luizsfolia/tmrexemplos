unit frmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Actions,
  Vcl.ActnList, uLmxInterfaces, uLmxInterfacesRegister, uLmxHelper;

type
  TfrmMainLmx = class(TForm)
    MainActionList: TActionList;
    RebuildDatabaseAction: TAction;
    actProducts: TAction;
    actCadClient: TAction;
    actOrder: TAction;
    Button1: TButton;
    Button3: TButton;
    Button4: TButton;
    Button2: TButton;
    procedure RebuildDatabaseActionExecute(Sender: TObject);
    procedure actProductsExecute(Sender: TObject);
    procedure actCadClientExecute(Sender: TObject);
    procedure actOrderExecute(Sender: TObject);
  private
    FConnection: ILmxConnection;
//    FSession: TSession;

    procedure DoBuildDatabase;
    procedure DoCheckEntities;
  public
    property Connection: ILmxConnection read FConnection;
//    property Session: TSession read FSession;
  public
    { Public declarations }
  end;

var
  frmMainLmx: TfrmMainLmx;

implementation

uses
  uLmxConexaoSqlite, uLmxConexao, uLmxDriverConexaoFireDac, uLmxClientModel,
  uLmxOrderModel, uLmxProductModel, uLmx.Service.DataBase, uLmxControleConexao,
  uLmxMetaData, ViewFindDefault, Vcl.ComCtrls, ViewCadProduct, ViewCadClient,
  ViewOrder;

{$R *.dfm}

procedure TfrmMainLmx.actCadClientExecute(Sender: TObject);
begin
  TfrmFindDefault.Select<TClient>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'ID';
      Sender.Columns.Add.Caption := 'Name';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TClient)
    begin
      Item.Caption := pItemObject.ID.ToString;
      Item.SubItems.Add(pItemObject.Name);
    end,
    'NAME',
    TfrmCadClient,
    Self);
end;

procedure TfrmMainLmx.actOrderExecute(Sender: TObject);
begin
  TfrmFindDefault.Select<TOrders>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'ID';
      Sender.Columns.Add.Caption := 'Date';
      Sender.Columns.Add.Caption := 'Number';
      Sender.Columns.Add.Caption := 'Total';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TOrders)
    begin
      Item.Caption := pItemObject.ID.ToString;
      Item.SubItems.Add(DateToStr(pItemObject.Date));
      Item.SubItems.Add(pItemObject.Number.ToString);
//      Item.SubItems.Add(IntToStr(pItemObject.Quantity));
    end,
    'Number',
    TfrmOrder,
    Self);
end;

procedure TfrmMainLmx.actProductsExecute(Sender: TObject);
begin
  TfrmFindDefault.Select<TProduct>(FConnection,
    procedure(Sender: TListView)
    begin
      Sender.Columns.Add.Caption := 'Name';
      Sender.Columns.Add.Caption := 'Price';
      Sender.Columns.Add.Caption := 'Quantity';
    end,
    procedure(Sender: TObject; Item: TListItem; pItemObject : TProduct)
    begin
      Item.Caption := pItemObject.Name;
      Item.SubItems.Add(CurrToStr(pItemObject.Price));
      Item.SubItems.Add(IntToStr(pItemObject.Quantity));
    end,
    'Name',
    TfrmCadProduct,
    Self);

end;

procedure TfrmMainLmx.DoBuildDatabase;
var
  lConexao: TLmxConexaoSqLite;
begin
  uLmxConexao.RegistrarDriverConexao(TLmxDriverConexaoFireDac.Create(nil));

  // Registros de Tabelas
  RegisterInterface.Tabelas.Registrar(TClient, 'Client');
  RegisterInterface.Tabelas.Registrar(TProduct, 'Product');
  RegisterInterface.Tabelas.Registrar(TOrders, 'Orders');

  TContextDataBaseConfig.Default.RegistrarConexao(TLmxConexaoSqLite,
    procedure (const pControleConexao : TLmxControleConexao)
    begin
      pControleConexao.HostName := 'localhost';
      pControleConexao.DataBase :=  '.\Data.db3';
      pControleConexao.ClasseDriver := TLmxConexaoSqLite.ClassName;
      pControleConexao.User_Name := 'admin';
      pControleConexao.Password := '';

      RegistrarConexao(TLmxConexaoSqLite, pControleConexao);

      // Cria��o / Atualiza��o do Banco de Dados
      lConexao := TLmxConexaoSqLite.Create;
      try
        lConexao.ConfigurarConexao(pControleConexao);

        LmxMetadata.SetConexao(lConexao);
        LmxMetadata.ExecutarNoBancoDeDados := True;
//        LmxMetadata.OnAlteracaoDataBaseEvent := OnAlterarBancoDados;
        LmxMetadata.CriarDataBase;

        if (not LmxMetadata.TemTelaRegistrada) then
          LmxMetadata.AtualizarTabelasRegistradas;
      finally
        lConexao.Free;
      end;

    end);

  FConnection := LmxConexao.Connection;

end;

procedure TfrmMainLmx.DoCheckEntities;
begin

end;

procedure TfrmMainLmx.RebuildDatabaseActionExecute(Sender: TObject);
begin
  DoBuildDatabase;
end;

end.
