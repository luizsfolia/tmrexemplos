unit uLmxProductModel;

interface

uses
  uLmxCore, uLmxAttributes, System.Math, Generics.Collections, System.Classes;

type

  [TLmxAttributeMetadata]
  TProduct = class(TBaseTabelaPadrao)
  private
    FName: string;
    FPrice: Double;
    fCreationTime: TTime;
    fCreationDateTime: TDateTime;
    FQuantity: Integer;
    fCreationDate: TDateTime;
  public
    [TLmxAttributeMetadata(50)]
    property Name : string read FName write FName;
    [TLmxAttributeMetadata]
    property Price: Double read FPrice write FPrice;
    [TLmxAttributeMetadata]
    property Quantity: Integer read FQuantity write FQuantity;
    [TLmxAttributeMetadata]
    property CreationDate: TDateTime read fCreationDate write fCreationDate;
    [TLmxAttributeMetadata]
    property CreationDateTime: TDateTime read fCreationDateTime write fCreationDateTime;
    [TLmxAttributeMetadata]
    property CreationTime: TTime read fCreationTime write fCreationTime;
  end;

  TProducts = class(TBaseList<TProduct>);

implementation

end.
