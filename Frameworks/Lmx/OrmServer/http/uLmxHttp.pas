unit uLmxHttp;

interface

uses
  uLmxHttpServer, uLmxHelper, uLmxAttributes, uLmx.Http.Base, System.Classes, uLmxClientModel,
  uIntfContext, uLmxCore, uLmxProductModel, uLmxOrderModel, uLmxOrderItemModel;

type

  THttpClient = class(THttp<TClient,IContextClient, TBaseList<TClient>>);

  THttpProduct = class(THttp<TProduct,IContextProduct, TBaseList<TProduct>>);

  THttpOrder = class(THttp<TOrders,IContextOrder, TBaseList<TOrders>>)
  public
    function GetById(pContext : IContextOrder; const Id : Integer) : ILmxRetorno<TOrders>; override;

    [HttpGet]
    [TLmxAttributeComando('/{Id}/Itens')]
    function ListByOrder(
      [FromServices] pContext : IContextOrder;
      [FromServices] pContextItens : IContextOrderItem;
      [FromParams] const Id : Integer) : ILmxRetorno<TOrderItensList>;
  end;

  THttpOrderItem = class(THttp<TOrderItem,IContextOrderItem, TBaseList<TOrderItem>>)

  public
    [HttpGet]
    [TLmxAttributeComando('/ListByOrder/{Id}')]
    function ListByOrder(
      [FromServices] pContext : IContextOrderItem;
      [FromParams] const Id : Integer) : ILmxRetorno<TOrderItensList>;
  end;


implementation


{ THttpOrderItem }

function THttpOrderItem.ListByOrder(pContext: IContextOrderItem;
  const Id: Integer): ILmxRetorno<TOrderItensList>;
begin
  Result := TLmxRetorno<TOrderItensList>.Create(
    pContext.ListByOrder(id));
end;

{ THttpOrder }


function THttpOrder.GetById(pContext: IContextOrder;
  const Id: Integer): ILmxRetorno<TOrders>;
begin
  Result := inherited ;
  pContext.GetItens(Id, REsult.Retorno.Itens);
end;

function THttpOrder.ListByOrder(pContext: IContextOrder;
  pContextItens: IContextOrderItem;
  const Id: Integer): ILmxRetorno<TOrderItensList>;
begin
  Result := TLmxRetorno<TOrderItensList>.Create(
    pContextItens.ListByOrder(id));
end;


end.
