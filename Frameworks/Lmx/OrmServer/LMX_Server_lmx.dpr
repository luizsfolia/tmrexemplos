program LMX_Server_lmx;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  uLmxHttpServer,
  uLmxAttributes,
  uLmxConexao,
  uLmxHelper,
  uLmxInterfaces,
  uLmxCmd,
  uLmxMetaData,
  uLmxInterfacesRegister,
  uLmxConexaoSqlite,
  uLmxDriverConexaoFireDac,
  uLmx.Service.DataBase,
  uLmxOrderModel in 'model\uLmxOrderModel.pas',
  uLmxProductModel in 'model\uLmxProductModel.pas',
  uLmxClientModel in 'model\uLmxClientModel.pas',
  uLmxControleConexao,
  uLmxHttp in 'http\uLmxHttp.pas',
  uIntfContext in 'context\uIntfContext.pas',
  uContext in 'context\uContext.pas',
  uLmxOrderItemModel in 'model\uLmxOrderItemModel.pas', IdCustomHTTPServer;

var
  FServer : TLmxHttpServer;
  FConexao : TLmxConexaoSqLite;

begin
  try

  ReportMemoryLeaksOnShutdown := True;

  uLmxConexao.RegistrarDriverConexao(TLmxDriverConexaoFireDac.Create(nil));

  // Registros de Tabelas
  RegisterInterface.Tabelas.Registrar(TClient, 'Client');
  RegisterInterface.Tabelas.Registrar(TProduct, 'Product');
  RegisterInterface.Tabelas.Registrar(TOrders, 'Orders');
  RegisterInterface.Tabelas.Registrar(TOrderItem, 'OrderItens');

  TContextDataBaseConfig.Default.RegistrarConexao(TLmxConexaoSqLite,
    procedure (const pControleConexao : TLmxControleConexao)
    begin
      pControleConexao.HostName := 'localhost';
      pControleConexao.DataBase :=  '.\Data.db3';
      pControleConexao.ClasseDriver := TLmxConexaoSqLite.ClassName;
      pControleConexao.User_Name := 'admin';
      pControleConexao.Password := '';

      RegistrarConexao(TLmxConexaoSqLite, pControleConexao);

      // Cria��o / Atualiza��o do Banco de Dados
      FConexao := TLmxConexaoSqLite.Create;
      try
        FConexao.ConfigurarConexao(pControleConexao);

        LmxMetadata.SetConexao(FConexao);
        LmxMetadata.ExecutarNoBancoDeDados := True;
//        LmxMetadata.OnAlteracaoDataBaseEvent := OnAlterarBancoDados;
        LmxMetadata.CriarDataBase;

        if (not LmxMetadata.TemTelaRegistrada) then
          LmxMetadata.AtualizarTabelasRegistradas;
      finally
        FConexao.Free;
      end;

      DesregistrarConexao;

    end);

    FServer := TLmxHttpServer.Create;

    FServer.AdicionarService<IContextClient>(
    IContextClient,
    function : IContextClient
    begin
      Result := TContextClient.Create;
    end);

    FServer.AdicionarService<IContextProduct>(
    IContextProduct,
    function : IContextProduct
    begin
      Result := TContextProduct.Create;
    end);

    FServer.AdicionarService<IContextOrder>(
    IContextOrder,
    function : IContextOrder
    begin
      Result := TContextOrder.Create;
    end);

    FServer.AdicionarService<IContextOrderItem>(
    IContextOrderItem,
    function : IContextOrderItem
    begin
      Result := TContextOrderItem.Create;
    end);



    FServer.AdicionarComando(THttpClient, '/Clients');
    FServer.AdicionarComando(THttpProduct, '/Products');
    FServer.AdicionarComando(THttpOrder, '/Orders');
    FServer.AdicionarComando(THttpOrderItem, '/OrderItens');


    Writeln('starting server at port 8500');

    FServer.Ativar(8500);

    Writeln('server online port 8500');

    Readln;

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
