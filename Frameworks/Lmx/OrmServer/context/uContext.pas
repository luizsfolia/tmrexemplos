unit uContext;

interface

uses
  uIntfContext, uLmx.Context.DataBase, uLmxClientModel, uLmxProductModel,
  uLmxOrderModel, uLmxInterfaces, uLmxComandoSql, uLmxOrderItemModel;

type

  TContextClient = class(TContextDataBase<TClient>, IContextClient);

  TContextProduct = class(TContextDataBase<TProduct>, IContextProduct);

  TContextOrder = class(TContextDataBase<TOrders>, IContextOrder)
  public
    function ById(const pId : Integer) : TOrders; override;
    procedure GetItens(const pOrderId : Integer; const pList : TOrderItensList);
  end;

  TContextOrderItem = class(TContextDataBase<TOrderItem>, IContextOrderItem)
  public
    function ListByOrder(const pOrderId : Integer) : TOrderItensList;
  end;

implementation

uses
  uLmxComandoDefault, System.SysUtils;


{ TContextOrderItem }

function TContextOrderItem.ListByOrder(
  const pOrderId: Integer): TOrderItensList;
begin
  Result := TOrderItensList.Create;
  TLmxComandoBaseDefaultList<TOrderItensList, TOrderItem>.CriarEExecutar(Result, function : TOrderITem begin Result := TOrderITem.Create end, nil, Self.ObterConexao, 'order_id = ' + IntToStr(pOrderId), Self.GetRequisicaoCliente);
end;

{ TContextOrder }

function TContextOrder.ById(const pId: Integer): TOrders;
begin
  Result := inherited;
  TLmxComandoBaseDefault<TClient>.CriarEExecutar(Result.Client_id, Result.Client, nil, Self.ObterConexao);
end;

procedure TContextOrder.GetItens(const pOrderId: Integer;
  const pList: TOrderItensList);
begin
  TLmxComandoBaseDefaultList<TOrderItensList, TOrderItem>.CriarEExecutar(pList, function : TOrderITem begin Result := TOrderITem.Create end, nil, Self.ObterConexao, 'order_id = ' + IntToStr(pOrderId), Self.GetRequisicaoCliente);
end;

end.
