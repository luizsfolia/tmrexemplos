unit uIntfContext;

interface

uses
  uLmx.Context.DataBase, uLmxClientModel, uLmxProductModel, uLmxOrderModel,
  uLmxOrderItemModel;


type

  IContextClient = interface(IContextDataBase<TClient>)
  ['{9DC0C993-09A3-4FA1-8892-A7BC589C1465}']
  end;


  IContextProduct = interface(IContextDataBase<TProduct>)
  ['{141F1CE7-7F97-45A5-85AD-056F7A56360D}']
  end;

  IContextOrder = interface(IContextDataBase<TOrders>)
  ['{70AE4532-E35E-47E4-8407-A1180E08FB88}']
    procedure GetItens(const pOrderId : Integer; const pList : TOrderItensList);
  end;

  IContextOrderItem = interface(IContextDataBase<TOrderItem>)
  ['{FEEA313D-8352-444D-873E-D07268B79A77}']
    function ListByOrder(const pOrderId : Integer) : TOrderItensList;
  end;

implementation


end.
